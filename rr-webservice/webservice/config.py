import os

hello_world_answer = "Hi."

if not 'MONGO_URI' in os.environ:
    MONGO_URI = 'mongodb://mongodb/hr_webservice'
else:
    MONGO_URI = os.environ['MONGO_URI']
