from flask import jsonify, request, g, Blueprint
from functools import wraps
import simplejson as json
import datetime
from dateutil import parser
from pprint import pprint as pp
import rrjobs
from bson import ObjectId
import bson

from app import app, mongo, jsonp

from config import hello_world_answer as hwa

api = Blueprint('api', __name__)

@api.route('/hello')
def hello_world():
    return jsonify({
        "message": hwa
    })

def login_required(f):
    @wraps(f)
    def wrapped(*args, **kwargs):

        try:
            name, auth = request.headers.get("Authentication").split(":")

        except ValueError as e:
            return jsonify({
                "message": "Include a header named authentication in your request in the form [screen_name]:[authentication]"
            }), 401

        user = mongo.db.users.find_one({
            'screen_name': name,
            'authentication': auth
        })

        if not user:
            return jsonify({
                'message': 'Wrong authentication code.'
            }), 401

        g.user = user

        return f(*args, **kwargs)
    return wrapped

# CREATE users

@api.route('/users', methods = ["POST"])
def create_user():

    data = request.json if request.json else dict()
    data.update(request.form)

    if not 'screen_name' in data and 'authentication' in data:
        return jsonify({
            'message': 'Include a screen_name and authentication field'
        }), 400

    screen_name = data['screen_name']
    authentication = data['authentication']

    users = mongo.db.users
    questions = mongo.db.questions
    questionnaires = mongo.db.questionnaires
    # if user already exists, return bad

    if users.find_one({'screen_name': screen_name}) or users.find_one({'authentication': authentication}):
        return jsonify({
            'message': 'User with name %s or authentication %s already exists.' % (screen_name, authentication)
        }), 400

    # create user

    uid = users.insert({
        'screen_name': screen_name,
        'authentication': authentication
    })

    # schedule all questions from question_templates immediately
    # skipe the open questions though

    qid = rrjobs.create_questionnaire(uid, 'intake')

    return jsonify({
        "screen_name": screen_name,
        "authentication": authentication,
        "uid": str(uid),
        "intake_id": str(qid)
    }), 201

@api.route('/users', methods = ["GET"])
@jsonp
def list_users():

    max_ = 20
    users = list(mongo.db.users.find({}, {'authentication': False}).limit(max_))

    for u in users: u['_id'] = str(u['_id'])

    return jsonify({
        'users': users
    })

@api.route('/users/<string:screen_name>')
@login_required
@jsonp
def get_user(screen_name):

    if not 'user' in g or g.user["screen_name"] != screen_name:
        return jsonify({
            'message': 'Not authenticated.'
        }), 403

    user = g.user
    user['_id'] = str(user['_id'])
    return jsonify({
        'user': user
    })

@api.route('/question_templates')
@jsonp
def list_question_templates():

    qt = list(mongo.db.question_templates.find())
    return jsonify({
        'question_templates': qt
    }), 200


@api.route('/users/<string:screen_name>/questionnaires')
@login_required
def get_schedule(screen_name):

    user = g.user
    if not user or user["screen_name"] != screen_name:
        return jsonify({
            'message': 'Not authenticated.'
        }), 403

    questionnaires = mongo.db.questionnaires

    qs = list(questionnaires.find({
        "screen_name": screen_name
    }))

    for q in qs:

        questions = list(mongo.db.questions.find({"_id" : {"$in": q["questions"]}}))
        q["questions"] = questions

        total = len(questions)
        completed = len([qst for qst in questions if "answer" in qst])

        q["completed"] = total == completed
        q["completed_fraction"] = float(completed) / total if total > 0 else 0

    return jsonify({
        "questionnaires": qs
    })

@app.route('/users/<string:screen_name>/create_questionnaire', methods = ["POST"])
def create_questionnaire(screen_name):


    data = request.json
    data.update(request.form)
    if not "category" in data or not data["category"] in rrjobs.QUESTIONNAIRE_TYPES:
        return jsonify({
            "message": "Include a 'category' variable with a value from the list %s." % (rrjobs.QUESTIONNAIRE_TYPES + ["intake"])
        }), 400

    user = mongo.db.users.find_one({'screen_name' : screen_name})
    if not user:
        return jsonify({
            "message" : "User with name %s does not exist." % screen_name
        }), 404

    category = data["category"]
    qid = rrjobs.create_questionnaire(user['_id'], category)

    return jsonify({
        "message": "Created questionnaire with category %s and id %s. " % (category, qid)
    })

@api.route('/questions/<string:question_id>', methods = ["PUT"])
@login_required
@jsonp
def add_answers(question_id):

    try:
        q = mongo.db.questions.find_one({'_id': ObjectId(question_id)})
    except bson.errors.InvalidId as e:
        return jsonify({
            'message': str(e)
        }), 400

    u = g.user

    if not q:
        return jsonify({
            'message': 'This question %s does not exist.' % (question_id)
        }), 404

    if not u or not q['user_id'] == u["_id"]:
        return jsonify({
            'message': 'Not authenticated.'
        }), 403

    data = request.json
    if not data or not ("answer" in data and "value" in data["answer"]):
        return jsonify({
            "message": "Include an answer in the request!"
        }), 400

    # check date format

    created_at = None
    if "created_at" in data["answer"]:
        try:
            created_at = parser.parse(data["answer"]["created_at"])
        except TypeError as e:
            return jsonify({
                "message": "Error parsing date string %s." % data["answer"]["created_at"]
            }), 400

    q["answer"] = {
        "value": data["answer"]["value"],
        "created_at": datetime.datetime.now() if not created_at else created_at
    }

    mongo.db.questions.update({"_id": q["_id"]}, q)

    return jsonify({
        'request_data': data,
        'q.answer': q["answer"],
        'message': 'Saved answer.'
    })

app.register_blueprint(api, url_prefix = '/api')
