#! /usr/bin/env python

import pymongo
import csv
from pprint import pprint as pp
import re
from config import MONGO_URI

def transform(row):

    q = {
        '_id': 'template_%s' % row['Variabele'],
        'title': row['Omschrijving'],
        'category': row['Variabele'].split('_')[0].lower() if '_' in row['Variabele'] else 'general'
    }

    options = [re.sub('[{}]', '', s) for s in row['Waarden'].split('}{') if s]
    if options:
        q['type'] = 'list'
        q['options'] = [{
            'value': int(o.split(',')[0]),
            'key': ','.join(o.split(',')[1:])
        } for o in options]
    else:
        q['type'] = 'open'

    return q

with open('resources/vragenlijst.csv') as f:
    reader = csv.DictReader(f, delimiter=',')
    rows = list(transform(r) for r in reader)

mongo = pymongo.MongoClient(MONGO_URI)

for r in rows:
    print(mongo.hr_webservice.question_templates.save(r))
