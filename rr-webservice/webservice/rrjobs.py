import logging
import schedule
import time
import pymongo
import bson, datetime
from config import MONGO_URI
import random

# connect to database from config value
db = pymongo.MongoClient(MONGO_URI)[MONGO_URI.split('/')[-1]]
QUESTIONNAIRE_TYPES = ["general", "psychote", "emote", "cogte", "autonomie", "burnout", "varwerk", "steunleider", "steuncollega"]

QUESTIONNAIRE_TITLES = {
    "general": "Algemeen",
    "psychote": "Psychische werklast",
    "emote": "Emotionele werklast",
    "cogte": "Cognitieve werklast",
    "autonomie": "Autonomie",
    "burnout": "Burnout gerelateerde klachten",
    "varwerk": "Werkvariatie",
    "steunleider": "Support van de leidinggevende",
    "steuncollega": "Support van je collega's",
    "intake": "Intake formulier"
}

def get_title(questionnaire_type): return QUESTIONNAIRE_TITLES[questionnaire_type]


log = logging.getLogger(__name__)

class rrException(Exception):
    pass

def daily_schedule():

    category = random.choice(QUESTIONNAIRE_TYPES)
    log.info('Applying daily schedule! This time, everyone gets a %s questionnaire. ')

    user_ids = [u['_id'] for u in db.users.find({}, {'_id': True})]
    for uid in user_ids:
        log.debug('Creating questionnaire %s for user %s.' % (category, uid))
        create_questionnaire(uid, category)

def heartbeat():
    log.debug('Scheduler reporting.')

# schedule due questionnaires

def main():

    # main function that should be started and handles the scheduling

    log.info('Starting scheduler ..')
    schedule.every(20).seconds.do(heartbeat)

    schedule.every().day.at("17:00").do(daily_schedule)

    while True:

        schedule.run_pending()
        time.sleep(1)

def create_questionnaire(user_id, questionnaire_type):

    if questionnaire_type == "intake":
        question_query = {'type': {'$exists': True}}
    elif questionnaire_type in QUESTIONNAIRE_TYPES:
        question_query = {'category': questionnaire_type}
    else:
        raise rrException('No questionnaire_type by name %s is known.' % questionnaire_type)

    intake = list(db.question_templates.find(question_query))
    user = db.users.find_one({'_id': bson.objectid.ObjectId(user_id)})

    qids = list()
    for q in intake:

        new_question = {
            "user_id": user['_id'],
            "screen_name": user['screen_name'],
            "due_date": datetime.datetime.now()
        }

        new_question.update(q)
        new_question['template_id'] = new_question.pop('_id')

        qids.append(db.questions.insert(new_question))

    qid = db.questionnaires.insert({
        'type': questionnaire_type,
        'title': get_title(questionnaire_type),
        'questions': qids,
        'user_id': user['_id'],
        'screen_name': user['screen_name'],
        'due_date': datetime.datetime.now()
    })

    return qid
