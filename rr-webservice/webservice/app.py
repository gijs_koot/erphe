from flask.json import JSONEncoder
from flask import Flask, jsonify, request, current_app, send_from_directory
from flask.ext.pymongo import PyMongo
from functools import wraps
from bson import ObjectId

from werkzeug.exceptions import default_exceptions
from werkzeug.exceptions import HTTPException

import logging
import os
from config import MONGO_URI

# start schedule

app = Flask(__name__, static_url_path = "/static")

app.config['MONGO_URI'] = MONGO_URI
@app.route("/<path:path>")
def index(path):
    return send_from_directory("static", "index.html")

@app.route("/api/<path:path>")
def unknown_api_route(path):
    return jsonify({
        "message": "Not found"
    }), 404


print('Connecting to MongoDB at %s.' % (MONGO_URI))

mongo = PyMongo(app)

def make_json_error(ex):
    response = jsonify(message=str(ex))
    response.status_code = (ex.code if isinstance(ex, HTTPException) else 500)
    return response

import datetime

class CustomJSONEncoder(JSONEncoder):

    def default(self, obj):
        try:
            if isinstance(obj, datetime.datetime):
                obj = obj.replace(microsecond = 0)
                return obj.isoformat()
            if isinstance(obj, ObjectId):
                return str(obj)
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)

app.json_encoder = CustomJSONEncoder

for code in default_exceptions.keys():
    app.error_handler_spec[None][code] = make_json_error

def jsonp(func):
    """Wraps JSONified output for JSONP requests."""
    @wraps(func)
    def decorated_function(*args, **kwargs):
        callback = request.args.get('callback', False)
        if callback:
            r = func(*args, **kwargs)
            if isinstance(r, tuple):
                data = r[0].data
            else:
                data = r.data
            content = str(callback) + '(' + data + ')'
            mimetype = 'application/javascript'
            return current_app.response_class(content, mimetype=mimetype)
        else:
            return func(*args, **kwargs)
    return decorated_function

import routes
