#! /usr/bin/env python3

import time
from app import app
from threading import Thread
import rrjobs
import logging

if __name__ == '__main__':

    logging.basicConfig(format='[%(levelname)s %(name)s] %(asctime)s - %(message)s', level = logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')

    # start a thread for the scheduler
    t = Thread(target = rrjobs.main)
    t.daemon = True
    t.start()

    # start the flask app
    # app.run(debug = True, use_reloader = False, host='0.0.0.0')

    time.sleep(1000)
