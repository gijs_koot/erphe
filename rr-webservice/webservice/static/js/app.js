var rrApp = angular.module('rrApp', ['ngRoute', 'ui.bootstrap']);

rrApp.config(['$routeProvider', function($routeProvider){

    $routeProvider.when('/users', {

        templateUrl: '/static/partials/user-list.html',
        controller: function($scope, $http){

            $http.get('/api/users').success(function(data){$scope.users = data;});

        }
    });

}]);
