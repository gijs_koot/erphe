Rapid Resilience webservice
---

#### Issues

* No issues at the moment

#### Changes

Breaking (Sorry..) | What | When | Why
--- | --- | --- | ---
No | Add daily scheduled random questionnaire at 17:00, a category is chosen randomly and then assigned to all users | 01-07 | Part of prototype
Yes | Also pass other types of questions, namely `open` ones from the questionnaires | 01 - 07 | Part of prototype
No | Update README.md | 23-06 | Wasn't accurate
Yes | Answers are part of the question, not of the questionnaire | 23-06 | Confusing

#### Endpoints

what | requires authentication | description
--- | --- | ---
`POST /users` | no | create user
`GET /users/:screen_name` | yes | get user details
`GET /users/:screen_name/questionnaires` | yes | get all questionnaires
`PUT /questions/:question_id` | yes | save answer to question | include 'value' ('created_at' is optional, defaults to now)
`GET /question_templates` | no | see all available question templates
`POST /users/:screen_name/create_questionnaire`  | no (!) | create a questionnaire from the templates provided (see endpoint above) | include a `category` value

### Question types

#### Scale

    {
        "type": "scale",
        "min": {
            "value": 0,
            "label": "Nee, geen pijn."
        },
        "max": {
            "value": 100,
            "label": "Ja, veel pijn."
        },
        "title": "Heb je veel pijn?",
        "description": "Op dit moment.",    // optional  
        "id": "234230d0df0",
        "template_id": "31251234",
        "answer": {                         // answer is optional
            "value": 100,
            "created_at": "12343243T213:2390Z"
        }
    }

#### Open

    {
        "_id": "template_Gevolgen",
        "category": "general",
        "title": "Wat zijn voor jou persoonlijk de gevolgen van het hebben van werkstress?",
        "type": "open"
      }

#### List

    {
        "type": "list",
        "title" "": ,
        "description":
        "options": [
            {
                "value": "aa3",
                "label": "Helemaal mee eens."
            },
            {
                ...
            }
        ],
        "id": "234234dsfsd"
    }
