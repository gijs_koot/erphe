## Devicemonitor

Dit is een webapplicatie waarmee een team onderzoekers kan werken aan een gezamenlijke database. De entiteiten waar de database voor bedoeld is, zijn oa. devices, sensoren, bijbehorende literatuur, ervaringen met devices en studies.

### Gebruik

De hele definitie staat in principe in de `Dockerfile` en `docker-compose`, dus om de applicatie in zijn geheel uit de grond te trekken kun je

`docker-compose up -d`

gebruiken.

#### Zonder `docker-compose`

Je kunt alles ook lokaal draaien, dan heb je MongoDB, Node.js en npm nodig.

```
mongod --dbpath [dbpath]                // zelf directory invullen
cd app                                  // naar de directory met de webapp gaan
npm install                             // installeer de dependencies
node index.js                           // run de server
```

De GUI vind je dan op `http://localhost:3000`. 
