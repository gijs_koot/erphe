#! /usr/bin/env python

import requests
import pymongo
import simplejson as json
import itertools

host = 'http://docker-coreos2:30001'

def upload_all():

    things = ["contexts", "domains", "emphasis", "platforms", "transmitters"]

    for thing in things:
        full_name = "sensor%s" % thing
        path = host + '/' + full_name
        with open("%s.txt" % full_name, 'Ur') as f:
            for l in f:
                option = l.lower().strip()
                requests.post(path, data = {
                    "name": option
                })

if __name__ == "__main__":

    upload_all()
