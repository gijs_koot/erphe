// adds paths and controllers for references

var dmReferences = angular.module('dmReferences', ['ngRoute', 'isteven-multi-select', 'ui.bootstrap']);

dmReferences.config(['$routeProvider', function($routeProvider){

    $routeProvider.when('/references', {
        templateUrl: 'partials/references-list.html',
        controller: function($scope, $http){
            console.log('Hello! References to be shown here. ');
            $http.get('/references').success(function(references){
                $scope.references = references;
            });

        }
    });

    $routeProvider.when('/references/new', {
        templateUrl: 'partials/reference-form.html',
        controller: referenceFormCtrl
    })

    $routeProvider.when('/references/edit/:reference_id', {
        templateUrl: 'partials/reference-form.html',
        controller: referenceFormCtrl
    });

}]);

var referenceFormCtrl = function($scope, $routeParams, $http, $location){

    // get devices to be added to the reference

    $http.get('/devices').success(function(data){
        $scope.devices = data;
        // todo: fix the correctly selected devices for the literature if any
        if($routeParams.reference_id == null){
            // init a new reference
            $scope.reference = {};
        } else {
            // get reference to be edited
            $http.get('/references/%%'.replace('%%', $routeParams.reference_id)).success(function(reference){
                $scope.reference = reference;
                $.each(reference.refersToDevices, function(index, device){
                    $.each($scope.devices, function(index, sdevice){
                        if(device.id == sdevice.id){
                            sdevice.selected = true;
                        }
                    });
                });
            });
        }

    });


    $scope.save = function(reference){
        $http.post('/references', reference).success(function(){
            $location.path('/references');
        }).error(function(error){
            $scope.error = error;
        });
    };

};
