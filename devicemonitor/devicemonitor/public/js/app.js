var dmApp = angular.module('dmApp', ['ngRoute', 'isteven-multi-select', 'ui.bootstrap', 'dmReferences']);

dmApp.config(['$routeProvider', function($routeProvider){

    $routeProvider.when('/devices', {
        templateUrl: 'partials/device-list.html',
        controller: function($scope, $http, $modal){

            $http.get('/devices').success(function(data){$scope.devices = data;});

            $scope.delete = function(device){

                var dlg = $modal.open({
                    templateUrl: 'partials/confirmModal.html',
                    controller: function($scope, $modalInstance){
                        $scope.close = function(){
                            $modalInstance.close();
                        };
                        $scope.thing = device;
                        $scope.ok = function(){
                            $http.delete('/devices/%%'.replace('%%', device.id)).success(function(){

                            });
                            $modalInstance.close();
                        };
                    }
                });

                dlg.result.then(function(){
                    $http.get('devices').success(function(data){$scope.devices = data;});
                });

            };

            $scope.popoverText = function(varname){
                return varname;
            };

        }
    });

    var deviceFormController = function($scope, $http, $location, $routeParams){

            var things = ['platforms', 'connections', 'dataexchangeprotocols', 'sensors', 'dataaccessibilities', 'feedbackmodes'];



            if($routeParams.device_id != null){
                $http.get('/devices/%%'.replace('%%', $routeParams.device_id)).success(function(data){
                    $scope.device = data;

                    things.map(function(thing){
                        $http.get('/%%'.replace('%%', thing)).success(function(data){
                            $scope[thing] = data;
                            $.each($scope.device[thing], function(index, thing){
                                $.each(data, function(index, sthing){
                                    if(sthing.id == thing.id){
                                        sthing.ticked = true;
                                    }
                                })
                            });
                        });
                    });

                });
            } else {
                things.map(function(thing){
                    $http.get('/%%'.replace('%%', thing)).success(function(data){
                        $scope[thing] = data;
                    });
                });
            }

            $scope.save = function(device){
                    $http.post('/devices', device).success(function(data){
                        console.log('Success.');
                        $location.path('/devices');
                    }).error(function(data){
                        $scope.error = data;
                    });
            };

        }

    $routeProvider.when('/devices/edit/:device_id', {

        templateUrl: 'partials/new-device.html',
        controller: deviceFormController

    });

    $routeProvider.when('/devices/new', {

        templateUrl: 'partials/new-device.html',
        controller: deviceFormController

    });

    $routeProvider.when('/platforms', {

        templateUrl: 'partials/platforms-list.html',
        controller: function($scope, $http, $location, $modal){

            $http.get('/platforms').success(function(data){
                $scope.platforms = data;
            });

            $scope.delete = function(platform){

                var dlg = $modal.open({
                    templateUrl: 'partials/confirmModal.html',
                    controller: function($scope, $modalInstance){
                        $scope.close = function(){
                            $modalInstance.close('ok');
                        };
                        $scope.thing = platform;
                        $scope.ok = function(){
                            $http.delete('/platforms/%%'.replace('%%', platform.id)).success(function(){
                                $http.get('platforms').success(function(data){$scope.devices = data;});
                            });
                            $modalInstance.close('ok');
                        };
                    }
                });

                dlg.result.then(function(){
                    $http.get('/platforms').success(function(data){$scope.platforms = data;});
                });


            };


        }

    });

    $routeProvider.when('/platforms/new', {

        templateUrl: 'partials/new-platform.html',
        controller: function($scope, $http, $location){

            $scope.save = function(platform){
                    $http.post('/platforms', platform).success(function(data){
                        $location.path('/platforms');
                    }).error(function(data){
                        $scope.error = data;
                    });
            };


        }

    });

    var experienceFormCtrl = function($scope, $http, $location, $routeParams){

            $scope.contexts = [{name: 'lab', id: 0}, {name: 'operational', id: 1}];
            $http.get('/devices/%%'.replace('%%', $routeParams.device_id)).success(function(device){
                $scope.device = device;
            });

            if($routeParams.experience_id != null){
                $http.get('/experiences/%%'.replace('%%', $routeParams.experience_id)).success(function(data){
                  $scope.experience = data;
                  $.each($scope.contexts, function(index, value){
                    $.each(data.contexts, function(dbindex, dbvalue){
                      if(value.id==dbvalue.id){
                          value.selected = true;
                      }
                    });
                  });
                });
            } else {
                // initialize a new experience
                var e = {};
                $scope.experience = e;
                e.date = new Date();

            }

            $scope.save = function(experience){
                // attach device id and current date
                experience.deviceId = $scope.device.id;


                // save, go to device
                $http.post('/experiences', experience).success(function(){
                    $location.path('/devices');
                }).error(function(error){
                    $scope.error = error;
                });

            };
    }

    $routeProvider.when('/devices/:device_id/experiences/new', {

        templateUrl: 'partials/new-experience.html',
        controller: experienceFormCtrl

    });

    $routeProvider.when('/devices/:device_id/experiences/edit/:experience_id', {
        templateUrl: 'partials/new-experience.html',
        controller: experienceFormCtrl
    });

    $routeProvider.when('/devices/:device_id', {

        templateUrl: 'partials/device-detail.html',
        controller: function($scope, $http, $location, $routeParams){

            $http.get('/devices/%%'.replace('%%', $routeParams.device_id)).success(function(device){
                $scope.device = device;
                $http.get('/experiences?deviceId=' + device.id).success(function(experiences){
                    $scope.device.experiences = experiences;
                });
            });

        }

    });

    $routeProvider.when('/experiences', {

        templateUrl: 'partials/experience-list.html',
        controller: function($scope, $http){

            $http.get('/experiences').success(function(data){
                $scope.experiences = data;
            });

        }

    });

}]);

dmApp.factory('dmInfo', function($http){

    $http.get('/bla');

    map = {
        'device.image': 'An image of the device.',
        'device.name': 'The name of the device. Examples are "Apple Watch" or "Fitbit X334".'
    };

    var dmInfo = function(varName){
        return varName in map ? map[varName] : varName;
    };

    return dmInfo;
});

dmApp.run(function($rootScope, dmInfo){
    $rootScope.varInfoText = dmInfo;
});

dmApp.directive('varName', function(){
    return {
        templateUrl: 'partials/varName.html',
        scope: {
            tag: '@',
            show: '@'
        },
        controller: function($rootScope, $scope){
            $scope.varInfoText = $rootScope.varInfoText;
        }
    };
});
