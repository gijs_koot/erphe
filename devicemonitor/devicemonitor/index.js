var PORT = process.env.PORT || 3000;
var ENV = process.env.NODE_ENV || 'development';

var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server, {'log level': 0});

require('deployd').attach(server, {
    socketIo: io,  // if not provided, attach will create one for you.
    env: ENV,
    db: {host:'mongodb', port:27017, name:'test-app'}
});

// After attach, express can use server.handleRequest as middleware

app.post('/devicename-check', function(req, res){
    res.status(200).json({message: 'Hi.'});
});

app.use(server.handleRequest);



// start server
server.listen(PORT);
