package nl.tno.erphe.wpxebuddy.model;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.List;

import nl.tno.erphe.wpxebuddy.util.ISO8601;

/**
 * Created by Thymen Wabeke on 28-5-2015.
 */
public class Questionnaire {

    public final static String TAG = "Questionnaire";

    private long id = -1L;
    private String foreignId = "";
    private long dueTimestamp = -1L;
    private String type;

    private String title;

    private boolean completed;

    private boolean isUserNotified;
    private double completedFraction;
    private List<AbstractQuestion> questions;

    public final static String JSON_TAG_FOREIGN_ID = "_id";
    public final static String JSON_TAG_TYPE = "type";
    public final static String JSON_TAG_DUE = "due_date";
    public final static String JSON_TAG_TITLE = "title";
    public final static String JSON_TAG_COMPLETED = "completed";
    public final static String JSON_TAG_COMPLETED_FRACTION = "completed_fraction";

    public Questionnaire(long id, String foreignId, long dueTimestamp, boolean completed, double completedFraction,
                         String type, String title, List<AbstractQuestion> questions) {
        this.id = id;
        this.foreignId = foreignId;
        this.isUserNotified = false;
        this.dueTimestamp = dueTimestamp;
        this.completed = completed;
        this.completedFraction = completedFraction;
        this.type = type;
        this.title = title;
        this.questions = questions;
    }

    public Questionnaire(String foreignId, long dueTimestamp, boolean completed, double completedFraction,
                         String type, String title, List<AbstractQuestion> questions) {
        this.id = -1L;
        this.foreignId = foreignId;
        this.dueTimestamp = dueTimestamp;
        this.completed = completed;
        this.isUserNotified = false;
        this.completedFraction = completedFraction;
        this.type = type;
        this.title = title;
        this.questions = questions;
    }

    public Questionnaire(long id, String foreignId, long dueTimestamp, boolean completed,
                         double completedFraction, String type, String title, boolean isUserNotified) {
        this.id = id;
        this.foreignId = foreignId;
        this.dueTimestamp = dueTimestamp;
        this.type = type;
        this.title = title;
        this.isUserNotified = isUserNotified;
        this.completed = completed;
        this.completedFraction = completedFraction;
        this.questions = null;
    }

    public Questionnaire(JSONObject jsonQuestionnaire) throws JSONException {
        fromJson(jsonQuestionnaire);
        this.isUserNotified = false;
        this.questions = null;
        this.id = -1L;
    }

    public void fromJson(JSONObject jsonQuestionnaire) throws JSONException {
        foreignId = jsonQuestionnaire.getString(JSON_TAG_FOREIGN_ID);
        type = jsonQuestionnaire.getString(JSON_TAG_TYPE);
        title = jsonQuestionnaire.getString(JSON_TAG_TITLE);
        completed = jsonQuestionnaire.getBoolean(JSON_TAG_COMPLETED);
        completedFraction = jsonQuestionnaire.getDouble(JSON_TAG_COMPLETED_FRACTION);
        try {
            dueTimestamp = ISO8601.toTimesamp(jsonQuestionnaire.getString(JSON_TAG_DUE));
        } catch (ParseException e) {
            Log.d(TAG, "Could not parse date", e);
            throw new JSONException("Error occurred while converting timestamp");
        }

    }

    public boolean isUserNotified() {
        return isUserNotified;
    }

    public void setIsUserNotified(boolean isUserNotified) {
        this.isUserNotified = isUserNotified;
    }

    public String getForeignId() {
        return foreignId;
    }

    public void setForeignId(String foreignId) {
        this.foreignId = foreignId;
    }

    public long getDueTimestamp() {
        return dueTimestamp;
    }

    public void setDueTimestamp(long dueTimestamp) {
        this.dueTimestamp = dueTimestamp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<AbstractQuestion> getQuestions() {
        return questions;
    }

    public void setQuestions(List<AbstractQuestion> questions) {
        this.questions = questions;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public double getCompletedFraction() {
        return completedFraction;
    }

    public void setCompletedFraction(double completedFraction) {
        this.completedFraction = completedFraction;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Questionnaire{" +
                "id=" + id +
                ", foreignId=" + foreignId +
                ", dueTimestamp=" + dueTimestamp +
                ", type='" + type + '\'' +
                ", questions=" + questions +
                '}';
    }
}
