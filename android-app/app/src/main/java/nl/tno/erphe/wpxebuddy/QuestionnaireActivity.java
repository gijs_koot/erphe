package nl.tno.erphe.wpxebuddy;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.List;

import nl.tno.erphe.wpxebuddy.database.QuestionDAO;
import nl.tno.erphe.wpxebuddy.database.QuestionnaireDAO;
import nl.tno.erphe.wpxebuddy.model.AbstractQuestion;
import nl.tno.erphe.wpxebuddy.model.QuestionTypes;
import nl.tno.erphe.wpxebuddy.model.Questionnaire;

/**
 * This activity shows a single questionnaire.
 *
 * @author thymen
 */
public class QuestionnaireActivity extends AppCompatActivity {

    private static final String TAG = "QuestionnaireActivity";
    public static final String FOREIGN_ID_TAG = "foreign_id";

    /**
     * The number of pages (wizard steps) to show in this demo.
     */
    private int numPages = 0;

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private CustomViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    /**
     * The list of questions in this questionnaire
     */
    private List<AbstractQuestion> questionList;

    private QuestionnaireDAO questionnaireDAO;
    private QuestionDAO questionDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionnaire);

        // Init the DAO's
        questionnaireDAO = new QuestionnaireDAO(this);
        questionDAO = new QuestionDAO(this);

        // Init the questionnaire
        loadQuestionList(getIntent().getStringExtra(FOREIGN_ID_TAG));

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (CustomViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        swipeOff();
    }

    /**
     * Load the questionnaire
     */
    private void loadQuestionList(String foreignId){
        Log.d(TAG, "loadQuestionList(String foreignId = " + foreignId + " ) called");
        // Get all questionnaires
        questionnaireDAO.open();
        Questionnaire questionnaire = questionnaireDAO.getQuestionnaireByForeignId(foreignId);
        questionnaireDAO.close();

        questionDAO.open();
        questionnaire.setQuestions(questionDAO.getQuestionsByQuestionnaire(questionnaire));
        questionDAO.close();

        // Do some debugging
        Log.d(TAG, "Questionnaire " + questionnaire.getForeignId() + " has #" + questionnaire.getQuestions().size() + " questions");
        // Set the list of questions that will be showed in this activity
        questionList = questionnaire.getQuestions();

        // Set the number of pages in the activity to the number of questions
        numPages = questionList.size();
    }


    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }

    public ViewPager getViewPager() {
        return mPager;
    }

    public void moveNext() {
        //it doesn't matter if you're already in the last item
        mPager.setCurrentItem(mPager.getCurrentItem() + 1);
    }

    public void movePrevious() {
        //it doesn't matter if you're already in the first item
        mPager.setCurrentItem(mPager.getCurrentItem() - 1);
    }

    public void swipeOn() {
        mPager.setSwipeable(true);
    }

    public void swipeOff() {
        mPager.setSwipeable(false);
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            AbstractQuestionFragment questionFragment = null;
            Bundle bundle = new Bundle();
            bundle.putBoolean(AbstractQuestionFragment.HAS_NEXT_QUESTION_TAG, (position < (numPages - 1)));
            bundle.putBoolean(AbstractQuestionFragment.HAS_PREVIOUS_QUESTION_TAG, (position > 0));
            bundle.putInt(AbstractQuestionFragment.QUESTION_TYPE_TAG, questionList.get(position).getQuestionType());
            bundle.putSerializable(AbstractQuestionFragment.QUESTION_OBJECT_TAG, questionList.get(position));
            if (questionList.get(position).getQuestionType() == QuestionTypes.LIST) {
                questionFragment = new ListQuestionFragment();
            } else if (questionList.get(position).getQuestionType() == QuestionTypes.SCALE) {
                questionFragment = new ScaleQuestionFragment();
            } else if (questionList.get(position).getQuestionType() == QuestionTypes.TEXT) {
                questionFragment = new TextQuestionFragment();
            }
            questionFragment.setArguments(bundle);
            return questionFragment;

        }

        @Override
        public int getCount() {
            return numPages;
        }
    }
}