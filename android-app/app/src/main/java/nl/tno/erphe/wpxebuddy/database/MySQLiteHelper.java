package nl.tno.erphe.wpxebuddy.database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * A helper class that manages the creation of the app's database.
 * <p/>
 * The database has three tables:
 * <ul>
 * <li><bold>Questionnaires</bold> Every row represents a single questionnaire. A questionnaire
 * is defined as a sequence/block of questions.</li>
 * <li><bold>Questions</bold> Each row represents a single question. Each question is assigned to
 * a questionnaire. Each question can have one or multiple answers.</li>
 * <li><bold>Answers</bold> Each row represents a single answer. Each answer is assigned to a
 * question. Questions can have multiple answers. The latest (most recent answer) is the relevant
 * answer.</li>
 * </ul>
 * <p/>
 * Interaction with the tables (inserting, deleting and retrieving rows) is provided by several DAO's.
 *
 * @author wabeketr
 * @see android.database.sqlite.SQLiteOpenHelper
 */
public class MySQLiteHelper extends SQLiteOpenHelper {

    /**
     * The name of the database
     */
    private static final String DATABASE_NAME = "results.db";
    /**
     * The version of the database. If the database structure has changed, the version number should
     * be increased. Note that this will delete all existing data in the database.
     */
    private static final int DATABASE_VERSION = 7; // The version of the database

    /**
     * The name of the table that contains all scans.
     */
    public static final String TABLE_QUESTIONNAIRES = "questionnaires";
    /**
     * The name of the table that holds all subscans.
     */
    public static final String TABLE_QUESTIONS = "questions";
    /**
     * The name of the table that holds all subscans.
     */
    public static final String TABLE_ANSWERS = "answers";


    public static final String COLUMN_ID = "id";
    public static final String COLUMN_FOREIGN_ID = "fId";

    public static final String COLUMN_DUE_TIMESTAMP = "due";
    public static final String COLUMN_TYPE = "type";

    public static final String COLUMN_QUESTIONNAIRE_ID = "questionnaire_id";
    public static final String COLUMN_QUESTIONNAIRE_COMPLETED = "completed";
    public static final String COLUMN_QUESTIONNAIRE_TITLE = "title";
    public static final String COLUMN_QUESTIONNAIRE_NOTIFIED = "notified";
    public static final String COLUMN_QUESTIONNAIRE_COMPLETED_FRACTION = "completed_fraction";
    public static final String COLUMN_JSON_REPRESENTATION = "json";

    public static final String COLUMN_QUESTION_ID = "questionnaire_id";

    /**
     * The query string that creates the questionnaires table.
     */
    private static final String CREATE_QUESTIONNAIRES_TABLE = "create table " + TABLE_QUESTIONNAIRES + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_FOREIGN_ID + " text not null, "
            + COLUMN_QUESTIONNAIRE_COMPLETED + " integer not null, "
            + COLUMN_QUESTIONNAIRE_NOTIFIED + " integer not null, "
            + COLUMN_QUESTIONNAIRE_COMPLETED_FRACTION + " real not null, "
            + COLUMN_TYPE + " text not null, "
            + COLUMN_QUESTIONNAIRE_TITLE + " text not null, "
            + COLUMN_DUE_TIMESTAMP + " integer not null);";

    /**
     * The query string that creates the questions table.
     */
    private static final String CREATE_QUESTIONS_TABLE = "create table " + TABLE_QUESTIONS + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_FOREIGN_ID + " text not null, "
            + COLUMN_QUESTIONNAIRE_ID + " integer not null, "
            + COLUMN_TYPE + " integer not null, "
            + COLUMN_JSON_REPRESENTATION + " text not null);";

    /**
     * Default constructor.
     *
     * @param context
     */
    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(MySQLiteHelper.class.getName(), "Creating new tables");
        db.execSQL(CREATE_QUESTIONNAIRES_TABLE);
        db.execSQL(CREATE_QUESTIONS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MySQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUESTIONNAIRES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUESTIONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ANSWERS);
        onCreate(db);
    }
}
