package nl.tno.erphe.wpxebuddy.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import nl.tno.erphe.wpxebuddy.model.Questionnaire;

/**
 * Created by Thymen Wabeke on 29-5-2015.
 */
public class QuestionnaireDAO extends AbstractDAO {

    public static final String TAG = "QuestionnaireDAO";


    /**
     * Default constructor that initializes a DAO.
     *
     * @param context
     */
    public QuestionnaireDAO(final Context context) {
        super(context);
    }

    public long insertQuestionnaire(Questionnaire questionnaire){
        if(questionnaire.getForeignId() == null || questionnaire.getForeignId().isEmpty()){
            Log.w(TAG, "insertQuestionnaire() failed, cause no foreign id was set");
            return -1L;
        }
        if(questionnaire.getDueTimestamp() < 0L){
            Log.w(TAG, "insertQuestionnaire() failed, cause no due date was set");
            return -1L;
        }

        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_FOREIGN_ID, questionnaire.getForeignId());
        values.put(MySQLiteHelper.COLUMN_DUE_TIMESTAMP, questionnaire.getDueTimestamp());
        values.put(MySQLiteHelper.COLUMN_TYPE, questionnaire.getType());
        values.put(MySQLiteHelper.COLUMN_QUESTIONNAIRE_TITLE, questionnaire.getTitle());
        values.put(MySQLiteHelper.COLUMN_QUESTIONNAIRE_COMPLETED, questionnaire.isCompleted());
        values.put(MySQLiteHelper.COLUMN_QUESTIONNAIRE_COMPLETED_FRACTION, questionnaire.getCompletedFraction());
        values.put(MySQLiteHelper.COLUMN_QUESTIONNAIRE_NOTIFIED, questionnaire.isUserNotified());

        return database.insert(MySQLiteHelper.TABLE_QUESTIONNAIRES, null, values);
    }

    public boolean updateQuestionnaire(Questionnaire questionnaire){
        long id = questionnaire.getId();
        if (id == -1L) {
            Log.w(TAG, "updateQuestionnaire() failed, cause no ID was set.");
            return false;
        }

        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_FOREIGN_ID, questionnaire.getForeignId());
        values.put(MySQLiteHelper.COLUMN_DUE_TIMESTAMP, questionnaire.getDueTimestamp());
        values.put(MySQLiteHelper.COLUMN_TYPE, questionnaire.getType());
        values.put(MySQLiteHelper.COLUMN_QUESTIONNAIRE_TITLE, questionnaire.getTitle());
        values.put(MySQLiteHelper.COLUMN_QUESTIONNAIRE_COMPLETED, questionnaire.isCompleted());
        values.put(MySQLiteHelper.COLUMN_QUESTIONNAIRE_COMPLETED_FRACTION, questionnaire.getCompletedFraction());
        values.put(MySQLiteHelper.COLUMN_QUESTIONNAIRE_NOTIFIED, questionnaire.isUserNotified());

        int numRows =  database.update(MySQLiteHelper.TABLE_QUESTIONNAIRES, values, MySQLiteHelper.COLUMN_ID
                + " = " + id, null);

        if(numRows != 1) {
            Log.i(TAG, "updateQuestionnaire() for Questionnaire.getId() = " + questionnaire.getId() +
                    " affected " + numRows + " row(s) (where 1 row was expected)");
            return false;
        }
        return true;
    }

    public Questionnaire getQuestionnaireById(long id){
        List<Questionnaire> questionnaires = new ArrayList<>();

        Cursor cursor = database.rawQuery("SELECT * FROM " + MySQLiteHelper.TABLE_QUESTIONNAIRES
                        + " WHERE " + MySQLiteHelper.COLUMN_ID + " = ?;",
                new String[]{String.valueOf(id)});

        if(cursor == null || cursor.getCount() == 0) {
            Log.d(TAG, "getQuestionnaireById( id = " + id + " ) returned no results") ;
            return null;
        }
        if(cursor.getCount() != 1) {
            Log.d(TAG, "getQuestionnaireById( id = " + id + " ) returned "  + cursor.getCount()
                    + " row(s) (where 1 row was expected)");
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Questionnaire questionnaire = cursorToQuestionnaire(cursor);
            questionnaires.add(questionnaire);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();

        return questionnaires.get(questionnaires.size()-1);
    }

    public Questionnaire getQuestionnaireByForeignId(String foreignId){
        List<Questionnaire> questionnaires = new ArrayList<>();

        Cursor cursor = database.rawQuery("SELECT * FROM " + MySQLiteHelper.TABLE_QUESTIONNAIRES
                        + " WHERE " + MySQLiteHelper.COLUMN_FOREIGN_ID + " = ?;",
                new String[]{String.valueOf(foreignId)});

        if(cursor == null || cursor.getCount() == 0) {
            Log.d(TAG, "getQuestionnaireByForeignId( foreignId = " + foreignId
                    + " ) returned no results") ;
            return null;
        }
        if(cursor.getCount() != 1) {
            Log.d(TAG, "getQuestionnaireByForeignId( foreignId = " + foreignId + " ) returned "
                    + cursor.getCount() + " row(s) (where 1 row was expected)");
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Questionnaire questionnaire = cursorToQuestionnaire(cursor);
            questionnaires.add(questionnaire);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();

        return questionnaires.get(questionnaires.size()-1);
    }

    public List<Questionnaire> getQuestionnairesByDueDate(long timeFrameStart, long timeFrameEnd){
        List<Questionnaire> questionnaires = new ArrayList<>();

        Cursor cursor = database.rawQuery("SELECT * FROM " + MySQLiteHelper.TABLE_QUESTIONNAIRES
                        + " WHERE " + MySQLiteHelper.COLUMN_DUE_TIMESTAMP + " >= ? AND "
                        + MySQLiteHelper.COLUMN_DUE_TIMESTAMP + " <= ? "
                        + "ORDER BY "+ MySQLiteHelper.COLUMN_DUE_TIMESTAMP + " DESC;",
                new String[]{String.valueOf(timeFrameStart), String.valueOf(timeFrameEnd)});

        if(cursor == null) {
            Log.d(TAG, "getQuestionnairesByDueDate( timeFrameStart = " + timeFrameStart
                    + ", timeFrameEnd = " + timeFrameEnd + " ) returned no results") ;
            return null;
        }
//        if(cursor.getCount() != 1) {
//            Log.d(TAG, "getQuestionnairesByDueDate( timeFrameStart = " + timeFrameStart
//                    + ", timeFrameEnd = " + timeFrameEnd + " ) returned "
//                    + cursor.getCount() + " row(s) (where 1 row was expected)");
//        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Questionnaire questionnaire = cursorToQuestionnaire(cursor);
            questionnaires.add(questionnaire);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();

        return questionnaires;
    }

    private Questionnaire cursorToQuestionnaire(Cursor cursor) {
        return new Questionnaire(
                cursor.getLong(cursor.getColumnIndex(MySQLiteHelper.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_FOREIGN_ID)),
                cursor.getLong(cursor.getColumnIndex(MySQLiteHelper.COLUMN_DUE_TIMESTAMP)),
                (cursor.getInt(cursor.getColumnIndex(MySQLiteHelper.COLUMN_QUESTIONNAIRE_COMPLETED)) == 1) ? true : false,
                cursor.getDouble(cursor.getColumnIndex(MySQLiteHelper.COLUMN_QUESTIONNAIRE_COMPLETED_FRACTION)),
                cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_TYPE)),
                cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_QUESTIONNAIRE_TITLE)),
                (cursor.getInt(cursor.getColumnIndex(MySQLiteHelper.COLUMN_QUESTIONNAIRE_NOTIFIED)) == 1) ? true : false);
    }
}
