package nl.tno.erphe.wpxebuddy.receiver;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import nl.tno.erphe.wpxebuddy.service.GetDataService;

public class StartSyncReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // This is the Intent to deliver to our service.
        Intent service = new Intent(context, GetDataService.class);

        // Start the service, keeping the device awake while it is launching.
        Log.i("StartSyncReceiver", "Starting service @ " + SystemClock.elapsedRealtime());
        startWakefulService(context, service);
    }
}
