package nl.tno.erphe.wpxebuddy.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class   ScheduleEventsReceiver extends BroadcastReceiver {

    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;

    public ScheduleEventsReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.i("ScheduleEventsReceiver", "onReceive() called");

        alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intentSync = new Intent(context, StartSyncReceiver.class);
        alarmIntent = PendingIntent.getBroadcast(context, 0, intentSync, 0);

        alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME,
                AlarmManager.INTERVAL_HALF_HOUR,
                AlarmManager.INTERVAL_HALF_HOUR, alarmIntent);
    }
}
