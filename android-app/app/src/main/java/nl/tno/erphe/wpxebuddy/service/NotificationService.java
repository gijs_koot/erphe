package nl.tno.erphe.wpxebuddy.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import java.util.Calendar;
import java.util.List;

import nl.tno.erphe.wpxebuddy.QuestionnaireListActivity;
import nl.tno.erphe.wpxebuddy.R;
import nl.tno.erphe.wpxebuddy.database.QuestionnaireDAO;
import nl.tno.erphe.wpxebuddy.model.Questionnaire;

/**
 * Service that creates notifications for the user if new questionnaires are available.
 *
 * The service contains database operation and should preferably not been run in the main/UI thread.
 *
 */
public class NotificationService extends Service {

    public NotificationService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
       return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(areQuestionnairesAvailable()){
            sendNotification();
        }
        return START_STICKY;
    }

    /**
     * Check whether the user has novel questionnaires that need te be answered. A questionnaires
     * needs to be answered if its due date has passed. For each questionnaire, only one notification
     * will be sent.
     *
     * @return True if a novel questionnaire is available; False otherwise.
     */
    private boolean areQuestionnairesAvailable() {
        boolean needNotification = false;
        QuestionnaireDAO questionnaireDAO = new QuestionnaireDAO(this);
        questionnaireDAO.open();
        List<Questionnaire> questionnaireList =
                questionnaireDAO.getQuestionnairesByDueDate(0L, Calendar.getInstance().getTimeInMillis());

        if (questionnaireList != null) {
            for (Questionnaire questionnaire : questionnaireList) {
                if (!questionnaire.isCompleted() && !questionnaire.isUserNotified()) {
                    needNotification = true;
                    questionnaire.setIsUserNotified(true);
                    questionnaireDAO.updateQuestionnaire(questionnaire);
                }
            }
        }

        questionnaireDAO.close();

        return needNotification;
    }

    /**
     * Builds a notification to inform the user that a new questionnaire is available
     */
    private void sendNotification() {
        NotificationManager notificationManager = (NotificationManager) getApplicationContext()
                .getSystemService(Context.NOTIFICATION_SERVICE);

        // Prepare intent which is triggered if the notification is selected
        Intent intent = new Intent(getApplicationContext(),
                QuestionnaireListActivity.class);

        // Build the notification
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                getApplicationContext())
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.notification_content))
                .setTicker(getString(R.string.notification_ticker))
                .setAutoCancel(true)
                .setDefaults(
                        Notification.DEFAULT_SOUND
                                | Notification.DEFAULT_VIBRATE
                                | Notification.DEFAULT_LIGHTS);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(QuestionnaireListActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(intent);

        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(345,
                PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        // Remove all previous notifications
        notificationManager.cancel(345);
        // Send the notification
        notificationManager.notify(345,
                mBuilder.build());
    }

}
