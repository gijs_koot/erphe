package nl.tno.erphe.wpxebuddy.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Thymen Wabeke on 19-5-2015.
 */
public class TextQuestion extends AbstractQuestion implements Serializable {

    public final static String TAG = "TextQuestion";

    public final static int QUESTION_TYPE = QuestionTypes.TEXT;

    public TextQuestion(long id, String foreignId, String title, String description, Answer answer) {
        super(id, foreignId, title, description, answer);
    }

    public TextQuestion(JSONObject jsonQuestion) throws JSONException {
        super(jsonQuestion);
    }

    public TextQuestion(long id, JSONObject jsonQuestion) throws JSONException {
        super(id, jsonQuestion);
    }

    public TextQuestion(String foreignId, String title, String description) {
        super(foreignId, title, description);
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.accumulate(JSON_TAG_FOREIGN_ID, foreignId);
        jsonObject.accumulate(JSON_TAG_TYPE, QUESTION_TYPE);
        jsonObject.accumulate(JSON_TAG_TITLE, title);
        jsonObject.accumulate(JSON_TAG_DESCRIPTION, description);
        if(hasAnswer()){
            jsonObject.accumulate(JSON_TAG_ANSWER,answer);
        }
        return jsonObject;
    }

    @Override
    public void fromJSON(JSONObject jsonQuestion) throws JSONException {
        foreignId = jsonQuestion.getString(JSON_TAG_FOREIGN_ID);
        title = jsonQuestion.getString(JSON_TAG_TITLE);
        if(jsonQuestion.has(JSON_TAG_DESCRIPTION)) {
            description = jsonQuestion.getString(JSON_TAG_DESCRIPTION);
        }
        if(jsonQuestion.has(JSON_TAG_ANSWER)){
//            Log.d(TAG, jsonQuestion.getString(JSON_TAG_ANSWER));
            JSONObject jsonAnswer = new JSONObject(jsonQuestion.getString(JSON_TAG_ANSWER));
            answer = new Answer(jsonAnswer);
        }
    }

    @Override
    public int getQuestionType() {
        return QUESTION_TYPE;
    }
}
