package nl.tno.erphe.wpxebuddy.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Thymen Wabeke on 19-5-2015.
 */
public class ListQuestion extends AbstractQuestion implements Serializable {

    public final static String TAG = "ListQuestion";

    public final static int QUESTION_TYPE = QuestionTypes.LIST;
    private HashMap<Integer,String> answerOptions;

    public ListQuestion(long id, String foreignId, String title, String description, Answer answer) {
        super(id, foreignId, title, description, answer);
    }

    public ListQuestion(JSONObject jsonQuestion) throws JSONException {
        super(jsonQuestion);
    }

    public ListQuestion(long id, JSONObject jsonQuestion) throws JSONException {
        super(id, jsonQuestion);
    }

    public ListQuestion(String foreignId, String title, String description) {
        super(foreignId, title, description);
    }

    public HashMap<Integer, String> getAnswerOptions() {
        return answerOptions;
    }

    public void setAnswerOptions(HashMap<Integer, String> answerOptions) {
        this.answerOptions = answerOptions;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonOptions = new JSONArray();
        jsonObject.accumulate(JSON_TAG_FOREIGN_ID, foreignId);
        jsonObject.accumulate(JSON_TAG_TYPE, QUESTION_TYPE);
        jsonObject.accumulate(JSON_TAG_TITLE, title);
        jsonObject.accumulate(JSON_TAG_DESCRIPTION, description);
        for (int value : answerOptions.keySet()) {
            jsonOptions.put(new JSONObject()
                    .accumulate(JSON_TAG_VALUE,value)
                    .accumulate(JSON_TAG_KEY,answerOptions.get(value)));
        }
        jsonObject.accumulate(JSON_TAG_OPTIONS, jsonOptions);
        if(hasAnswer()){
            jsonObject.accumulate(JSON_TAG_ANSWER,answer);
        }
        return jsonObject;
    }

    @Override
    public void fromJSON(JSONObject jsonQuestion) throws JSONException {
        foreignId = jsonQuestion.getString(JSON_TAG_FOREIGN_ID);
        title = jsonQuestion.getString(JSON_TAG_TITLE);
        if(jsonQuestion.has(JSON_TAG_DESCRIPTION)) {
            description = jsonQuestion.getString(JSON_TAG_DESCRIPTION);
        }
        JSONArray jsonOptions = jsonQuestion.getJSONArray(JSON_TAG_OPTIONS);
        answerOptions = new HashMap<>();
        for (int i = 0; i < jsonOptions.length(); i++) {
            JSONObject option = jsonOptions.getJSONObject(i);
            answerOptions.put(option.getInt(JSON_TAG_VALUE),option.getString(JSON_TAG_KEY));
        }
        if(jsonQuestion.has(JSON_TAG_ANSWER)){
//            Log.d(TAG, jsonQuestion.getString(JSON_TAG_ANSWER));
            JSONObject jsonAnswer = new JSONObject(jsonQuestion.getString(JSON_TAG_ANSWER));
            answer = new Answer(jsonAnswer);
        }
    }

    @Override
    public int getQuestionType() {
        return QUESTION_TYPE;
    }
}
