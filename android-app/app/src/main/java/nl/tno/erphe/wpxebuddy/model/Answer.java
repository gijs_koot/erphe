package nl.tno.erphe.wpxebuddy.model;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;

import nl.tno.erphe.wpxebuddy.util.Api;
import nl.tno.erphe.wpxebuddy.util.ISO8601;
import nl.tno.erphe.wpxebuddy.util.UserProfile;

/**
 * Created by Thymen Wabeke on 19-5-2015.
 */
public class Answer implements Serializable {

    public final static String TAG = "Answer";

    private long timestamp;
    private String response;

    private final static String JSON_TAG_VALUE = "value";
    private final static String JSON_TAG_TIMESTAMP = "created_at";

    public Answer(long timestamp, String response) {
        this.timestamp = timestamp;
        this.response = response;
    }

    public Answer() {
        this(-1L,null);
    }

    public Answer(String response) {
        this(System.currentTimeMillis(), response);
    }

    public Answer(int response) {
        this(System.currentTimeMillis(), String.valueOf(response));
    }

    public Answer(JSONObject jsonObject) throws JSONException {
        fromJSON(jsonObject);
    }

    public JSONObject toJSON() throws JSONException{
        JSONObject jsonObject = new JSONObject();
        jsonObject.accumulate(JSON_TAG_VALUE, response);
        jsonObject.accumulate(JSON_TAG_TIMESTAMP, ISO8601.fromTimestamp(timestamp));
        return jsonObject;
    }

    public void fromJSON(JSONObject jsonAnswer) throws JSONException{
        response = jsonAnswer.getString(JSON_TAG_VALUE);
        try {
            timestamp = ISO8601.toTimesamp(jsonAnswer.getString(JSON_TAG_TIMESTAMP));
        } catch (ParseException e) {
            timestamp = -1L;
            Log.d(TAG, "Could not parse date", e);
            throw new JSONException("Error occurred while converting timestamp");
        }
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public int getResponseInt() {
        return Integer.valueOf(response);
    }

    public void setResponse(int response) {
        this.response = String.valueOf(response);
    }

    @Override
    public String toString(){
        try {
            return toJSON().toString();
        } catch (JSONException e) {
            Log.e(TAG,"Could not convert answer to JSON", e);
            return "Answer (Could not convert answer to JSON)";
        }
    }
}
