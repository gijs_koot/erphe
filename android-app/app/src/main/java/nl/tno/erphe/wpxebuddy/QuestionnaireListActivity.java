package nl.tno.erphe.wpxebuddy;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import nl.tno.erphe.wpxebuddy.database.QuestionnaireDAO;
import nl.tno.erphe.wpxebuddy.model.Questionnaire;
import nl.tno.erphe.wpxebuddy.service.GetDataService;
import nl.tno.erphe.wpxebuddy.service.MSBandService;

/**
 * This activity shows the list of questionnaires that is available for the user.
 *
 * @author thymen
 */
public class QuestionnaireListActivity extends AppCompatActivity {

    private static final String TAG = "QuestionnaireListActvt";

    MSBandService msBandService;
    boolean msBandServiceBound = false;

    private ListView listview;
    private Menu optionsMenu;

    private QuestionnaireDAO questionnaireDAO;
    private List<Questionnaire> questionnaires;

    private QuestionnaireAdapter questionnaireAdapter;
    private boolean isMSBandServiceRunning = false;

    /**
     * Listen for broadcasts that are sent when a sync with the webservice has finished (and new
     * questionnaires might be available.
     */
    private BroadcastReceiver syncFinishedReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            setRefreshActionButtonState(false);
            refreshList();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionnaire_list);
        initObjects();
    }

    private void initObjects() {
        if (questionnaireAdapter == null || questionnaireDAO == null) {
            questionnaireAdapter = new QuestionnaireAdapter(this, new ArrayList<Questionnaire>());
            listview = (ListView) findViewById(R.id.listview);
            listview.setAdapter(questionnaireAdapter);

            questionnaireDAO = new QuestionnaireDAO(this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Bind to MSBandService
        Intent intent = new Intent(this, MSBandService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        if(msBandServiceBound){
            msBandService.setActivity(this);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(syncFinishedReceiver,
                new IntentFilter(GetDataService.SYNC_FINISHED));
        refreshList();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(syncFinishedReceiver);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        if (msBandServiceBound) {
            unbindService(mConnection);
            msBandServiceBound = false;
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        initObjects();
    }

    /**
     * Back button listener.
     * Will close the application if the back button pressed.
     */
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to MSBandService, cast the IBinder and get MSBandService instance
            MSBandService.MSBandServiceBinder binder = (MSBandService.MSBandServiceBinder) service;
            msBandService = binder.getService();
            msBandServiceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            msBandServiceBound = false;
        }
    };

    /**
     * Refresh the list of questionnaires.
     *
     */
    private void refreshList() {
        Log.d(TAG, "refreshList() called");
        questionnaireAdapter.clear();

        questionnaireDAO.open();
        questionnaires = questionnaireDAO.getQuestionnairesByDueDate(0L,
                Calendar.getInstance().getTimeInMillis());
        questionnaireDAO.close();

        if (questionnaires != null) {
            Log.d(TAG, "There are #" + questionnaires.size() + " questionnaires due");

            for (Questionnaire q : questionnaires) {
                questionnaireAdapter.add(q);
            }

            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, final View view,
                                        int position, long id) {
                    // Get the questionnaire object that was clicked
                    Questionnaire selectedQuestionnaire = (Questionnaire) parent.getItemAtPosition(position);

                    // Create the intent for opening the clicked questionnaire
                    Intent openQuestionnaire = new Intent(getApplicationContext(),
                            QuestionnaireActivity.class);
                    openQuestionnaire.putExtra(QuestionnaireActivity.FOREIGN_ID_TAG,
                            selectedQuestionnaire.getForeignId()); // The clicked questionnaire is identified by its foreign id

                    startActivity(openQuestionnaire);
                }

            });
        } else {
            Log.d(TAG, "getQuestionnairesByDueDate() returned NULL");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.optionsMenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_sync:
                setRefreshActionButtonState(true);
                Intent iSync = new Intent(this, GetDataService.class);
                startService(iSync);
            break;
            case R.id.action_band:
                toggleMSBandService();
            break;
        }
        return true;

    }


    /**
     * Toggle streaming data from the Microsoft Band on or off
     */
    private void toggleMSBandService(){
        if(isMSBandServiceRunning){
            isMSBandServiceRunning = false;
            if(msBandServiceBound) {
                msBandService.stopMonitoring();
            }
            setBandActionButtonState(false);

        } else {
            isMSBandServiceRunning = true;
            if(msBandServiceBound) {
                msBandService.startMonitoring();
            }
            setBandActionButtonState(true);
        }
    }

    /**
     * Set the state of the Microsoft Band button (shown in the menu)
     *
     * @param connected
     */
    private void setBandActionButtonState(final boolean connected){
        if (optionsMenu != null) {

            final MenuItem bandItem = optionsMenu
                    .findItem(R.id.action_band);

            if (bandItem != null) {
                if (connected) {
                    bandItem.setTitle(R.string.action_band_disconnect);
                } else {
                    bandItem.setTitle(R.string.action_band_connect);
                }
            }
        }
    }

    /**
     * Set the state of the Refresh button (shown in the menu)
     *
     * @param refreshing
     */
    public void setRefreshActionButtonState(final boolean refreshing) {
        if (optionsMenu != null) {

            final MenuItem refreshItem = optionsMenu
                    .findItem(R.id.action_sync);

            if (refreshItem != null) {
                if (refreshing) {
                    refreshItem.setActionView(R.layout.actionbar_indeterminate_progress);
                } else {
                    refreshItem.setActionView(null);
                }
            }
        }
    }

}


