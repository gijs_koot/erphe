package nl.tno.erphe.wpxebuddy;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.security.MessageDigest;

import nl.tno.erphe.wpxebuddy.util.Api;
import nl.tno.erphe.wpxebuddy.util.UserProfile;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends Activity {

    private static final String AUTHENTICATION_URL = "http://144.76.75.238/erprapid/users";
    private static final String TAG = "LoginActivity";

    private UserLoginTask mAuthTask = null;
    private UserProfile userProfile;

    // UI references.
    private EditText mScreenNameView;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set up the login form.
        mScreenNameView = (EditText) findViewById(R.id.screen_name);
        mScreenNameView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mSignInButton = (Button) findViewById(R.id.sign_in_button);
        mSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        userProfile = new UserProfile(this);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mScreenNameView.setError(null);

        // Store values at the time of the login attempt.
        String screenName = mScreenNameView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid screenName address.
        if (TextUtils.isEmpty(screenName)) {
            mScreenNameView.setError(getString(R.string.error_field_required));
            focusView = mScreenNameView;
            cancel = true;
        } else if (!isScreenNameValid(screenName)) {
            mScreenNameView.setError(getString(R.string.error_invalid_screen_name));
            focusView = mScreenNameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(this,screenName);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isScreenNameValid(String email) {
        return (email.length() > 3 && !email.contains(" "));
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mScreenName;
        private Context context;
//        private final String mPassword;

        UserLoginTask(Context context, String screenName) {
            this.mScreenName = screenName;
            this.context = context;
//            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Integer responseCode = -1;
            String response = "";
            JSONObject responseJson;

            try {
                MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
                messageDigest.update(mScreenName.getBytes());
                String encryptedString = new String(messageDigest.digest());

                JSONObject jsonObject = new JSONObject();
//                HashMap<String, String> requestParams = new HashMap<>();
                jsonObject.accumulate("screen_name", mScreenName);
//                requestParams.put("authentication", encryptedString);
                jsonObject.accumulate("authentication", mScreenName+mScreenName);

                Api api = new Api();
                responseJson = api.doPostJson(AUTHENTICATION_URL, jsonObject);
                responseCode = api.getLastResponseCode();
                Log.d(TAG, "responseCode = " + responseCode);

                if(responseCode == HttpURLConnection.HTTP_CREATED){
                    try {
                        userProfile.login(
                                responseJson.getString("screen_name"),
                                responseJson.getString("authentication"),
                                responseJson.getString("uid"));
                        return true;
                    } catch (JSONException e) {
                        Log.d(TAG, "Could not get user profile data from responseJson", e);
                    }
                }
            } catch (Exception e) {
                Log.d(TAG, "An error occurred during login", e);
            }

            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                Log.d(TAG, "Successful login");
                Intent intent = new Intent(context, MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                mScreenNameView.setError(getString(R.string.error_general));
                mScreenNameView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}



