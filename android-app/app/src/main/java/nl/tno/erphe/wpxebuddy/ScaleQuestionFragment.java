package nl.tno.erphe.wpxebuddy;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import nl.tno.erphe.wpxebuddy.database.QuestionDAO;
import nl.tno.erphe.wpxebuddy.model.ScaleQuestion;
import nl.tno.erphe.wpxebuddy.util.Api;
import nl.tno.erphe.wpxebuddy.util.UserProfile;

/**
 * Created by Thymen Wabeke on 19-5-2015.
 */
public class ScaleQuestionFragment extends AbstractQuestionFragment {

    public static final String TAG = "ScaleQuestionFragment";
    private View rootView;
    private ScaleQuestion question;


    public ScaleQuestionFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_question_scale, container, false);
        mPager = ((QuestionnaireActivity) getActivity()).getViewPager();
        questionDAO = new QuestionDAO(getActivity());
        api = new Api();
        userProfile = new UserProfile(getActivity());
        processBundle();
        initQuestionObject();
        loadQuestionHeader(rootView,question);
        loadQuestionFooter(rootView, question);
        loadQuestionLabels();
        return rootView;
    }


    @Override
    protected void initQuestionObject() {
        question = (ScaleQuestion) getArguments().getSerializable(QUESTION_OBJECT_TAG);
        updateQuestionObject();
    }

    @Override
    protected void updateQuestionObject() {
        questionDAO.open();
        question = (ScaleQuestion) questionDAO.getQuestionByForeignId(question.getForeignId());
        questionDAO.close();
    }

//    @Override
//    protected void processAnswer() {
//        questionDAO.open();
//        questionDAO.updateQuestion(question);
//        questionDAO.close();
//        JSONObject jsonAnswer = new JSONObject();
//        try {
//            jsonAnswer.accumulate("answer", question.getAnswer().getResponse());
//            jsonAnswer.accumulate("answered_at", ISO8601.fromTimestamp(question.getAnswer().getTimestamp()));
//        } catch (JSONException e) {
//            Log.e(TAG, "Could not prepare JSON object", e);
//        }
//        String url = String.format("%s/questions/%s", Api.BASE_URL, question.getForeignId());
//        try {
//            String result = api.doPut(url, jsonAnswer, userProfile.getAuthentication());
//            Log.d(TAG, "Server response: " + result);
//        } catch (Exception e) {
//            Log.e(TAG, "Could not send answer to webservice");
//        }
//    }

    public void loadQuestionLabels() {
        TextView minLabelView = (TextView) rootView.findViewById(R.id.labelLeft);
        TextView maxLabelView = (TextView) rootView.findViewById(R.id.labelRight);

        minLabelView.setText(question.getMinLabel());
        maxLabelView.setText(question.getMaxLabel());

        SeekBar seekBar = (SeekBar) rootView.findViewById(R.id.seekBar);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar1, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar1) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar1) {
                nextButton.setEnabled(true);
                if (hasNextQuestion) {
                    ((QuestionnaireActivity) getActivity()).moveNext();
                }

            }
        });

    }

    @Override
    public void onClick(View v) {
        onClick(v, question);
    }

}
