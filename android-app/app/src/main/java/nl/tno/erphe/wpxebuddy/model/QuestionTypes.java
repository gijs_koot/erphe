package nl.tno.erphe.wpxebuddy.model;

/**
 * Created by Thymen Wabeke on 19-5-2015.
 */
public class QuestionTypes {
    public static final int LIST = 0;
    public static final int TEXT = 1;
    public static final int SCALE = 2;

    public static final String LIST_TAG = "list";
    public static final String TEXT_TAG = "open";
    public static final String SCALE_TAG = "scale";
}
