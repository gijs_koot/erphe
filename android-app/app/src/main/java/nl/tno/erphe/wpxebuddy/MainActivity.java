package nl.tno.erphe.wpxebuddy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import nl.tno.erphe.wpxebuddy.receiver.ScheduleEventsReceiver;
import nl.tno.erphe.wpxebuddy.service.GetDataService;
import nl.tno.erphe.wpxebuddy.util.UserProfile;


public class MainActivity extends AppCompatActivity {

    private UserProfile userProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userProfile = new UserProfile(this);
        if(userProfile.isAuthenticated()){
            // Start a synchronization
            Intent iSync = new Intent(this, GetDataService.class);
            startService(iSync);
            // Schedule automatic synchronization
            Intent iSchedule = new Intent(this, ScheduleEventsReceiver.class);
            sendBroadcast(iSchedule);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        openActivity();
    }

    /**
     * Start the right activity for the user (either login screen or list of questionnaires)
     */
    private void openActivity(){
        if(!userProfile.isAuthenticated()){
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else{
            Intent iList = new Intent(this, QuestionnaireListActivity.class);
            startActivity(iList);
        }
    }


}
