package nl.tno.erphe.wpxebuddy;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.tno.erphe.wpxebuddy.database.QuestionDAO;
import nl.tno.erphe.wpxebuddy.model.Answer;
import nl.tno.erphe.wpxebuddy.model.ListQuestion;
import nl.tno.erphe.wpxebuddy.util.Api;
import nl.tno.erphe.wpxebuddy.util.UserProfile;

/**
 * Created by Thymen Wabeke on 19-5-2015.
 */
public class ListQuestionFragment extends AbstractQuestionFragment {

    public static final String TAG = "ListQuestionFragment";

    private View rootView;
    private ListQuestion question;
    private ListView listview;

    public ListQuestionFragment() {
    }

    static Integer getKey(HashMap<Integer, String> map, String value) {
        Integer key = null;
        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            if ((value == null && entry.getValue() == null) || (value != null && value.equals(entry.getValue()))) {
                key = entry.getKey();
                break;
            }
        }
        return key;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        questionDAO = new QuestionDAO(getActivity());
        api = new Api();
        userProfile = new UserProfile(getActivity());

        processBundle();
        initQuestionObject();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView() called");
        rootView = inflater.inflate(R.layout.fragment_question_list, container, false);
        mPager = ((QuestionnaireActivity) getActivity()).getViewPager();

        loadQuestionHeader(rootView, question);
        loadQuestionFooter(rootView, question);

        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        updateQuestionObject();
        loadQuestionOptions();
        warnIfOffline(getActivity());
    }

    @Override
    protected void initQuestionObject() {
        question = (ListQuestion) getArguments().getSerializable(QUESTION_OBJECT_TAG);
        updateQuestionObject();
    }

    @Override
    protected void updateQuestionObject() {
        questionDAO.open();
        question = (ListQuestion) questionDAO.getQuestionByForeignId(question.getForeignId());
        questionDAO.close();
    }

    private void loadQuestionOptions() {
        Log.d(TAG, "loadQuestionOptions() called");
        listview = (ListView) rootView.findViewById(R.id.question_answer_list);

        final StableArrayAdapter adapter = new StableArrayAdapter(getActivity(),
                android.R.layout.simple_list_item_1, new ArrayList<>(question.getAnswerOptions().values()));

        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int j = 0; j < parent.getChildCount(); j++) {
                    parent.getChildAt(j).setBackgroundColor(Color.TRANSPARENT);
                }
                view.setBackgroundColor(Color.GRAY);

                final String item = (String) parent.getItemAtPosition(position);
                question.setAnswer(new Answer(getKey(question.getAnswerOptions(), item)));
                handleAnswer(question);

                nextButton.setEnabled(true);
                if(hasNextQuestion){
                    ((QuestionnaireActivity) getActivity()).moveNext();
                }

            }
        });
    }

    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final View view = super.getView(position, convertView, parent);

            view.setBackgroundColor(Color.TRANSPARENT);
            if(question != null && question.hasAnswer()) {
                    if(position == (question.getAnswer().getResponseInt()-1)) {
                        view.setBackgroundColor(Color.GRAY);
                    }
            }

            return view;
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }

    @Override
    public void onClick(View v) {
        onClick(v, question);
    }
}
