package nl.tno.erphe.wpxebuddy.service;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.microsoft.band.BandClient;
import com.microsoft.band.BandClientManager;
import com.microsoft.band.BandException;
import com.microsoft.band.BandInfo;
import com.microsoft.band.BandIOException;
import com.microsoft.band.ConnectionState;
import com.microsoft.band.UserConsent;
import com.microsoft.band.sensors.BandAccelerometerEvent;
import com.microsoft.band.sensors.BandAccelerometerEventListener;
import com.microsoft.band.sensors.BandCaloriesEvent;
import com.microsoft.band.sensors.BandCaloriesEventListener;
import com.microsoft.band.sensors.BandDistanceEvent;
import com.microsoft.band.sensors.BandDistanceEventListener;
import com.microsoft.band.sensors.BandGyroscopeEvent;
import com.microsoft.band.sensors.BandGyroscopeEventListener;
import com.microsoft.band.sensors.BandHeartRateEvent;
import com.microsoft.band.sensors.BandHeartRateEventListener;
import com.microsoft.band.sensors.BandSkinTemperatureEvent;
import com.microsoft.band.sensors.BandSkinTemperatureEventListener;
import com.microsoft.band.sensors.BandUVEvent;
import com.microsoft.band.sensors.BandUVEventListener;
import com.microsoft.band.sensors.HeartRateConsentListener;
import com.microsoft.band.sensors.SampleRate;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;
import org.influxdb.dto.Pong;

import java.util.concurrent.TimeUnit;

import nl.tno.erphe.wpxebuddy.QuestionnaireListActivity;
import nl.tno.erphe.wpxebuddy.R;
import nl.tno.erphe.wpxebuddy.util.UserProfile;


public class MSBandService extends Service {

    private static final String TAG = "MSBandService";
    private final IBinder mBinder = new MSBandServiceBinder();

    private InfluxDB influxDB;
    private static final String dbName = "test";
    private static final String serverAddr = "http://tnomedialab.nl/influxdb-erp";
    private static final String influxUsr = "root";
    private static final String influxPwd = "pwd4erp";

    private UserProfile userProfile;
    private Activity activity = null;
    private BandClient client = null;
    boolean started = false;

    public MSBandService() {
        super();
        influxDB = InfluxDBFactory.connect(serverAddr, influxUsr, influxPwd);
        influxDB.enableBatch(4000, 45, TimeUnit.MINUTES);

        Log.d(TAG, "Connecting to infuxDB server..");

        Thread t = new Thread(new Runnable() {
            public void run() {
                boolean influxDBstarted = false;
                do {
                    Pong response;
                    try {
                        response = influxDB.ping();
                        System.out.println(response);
                        if (!response.getVersion().equalsIgnoreCase("unknown")) {
                            influxDBstarted = true;
                            Log.i(TAG,"Connected");

                        }
                    } catch (Exception e) {
                        // NOOP intentional
                        Log.e(TAG,"Could not ping influxDB server", e);
                    }
                    try {
                        Thread.sleep(100L);
                    } catch (InterruptedException e) {
                        Log.e(TAG, "Could not go to sleep", e);
                    }
                } while (!influxDBstarted);

            }
        });
        t.start();
        Log.d(TAG,"Yes! There is a connection");
    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class MSBandServiceBinder extends Binder {

        public MSBandService getService() {
            // Return this instance of LocalService so clients can call public methods
            return MSBandService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    @Override
    public void onDestroy() {
        stopMonitoring();
    }

    public void setActivity(Activity activity){
        this.activity = activity;
    }

    public void startMonitoring(){
        userProfile = new UserProfile(this);
        if(!started){
            displayNotification();
            started = true;
            new MonitoringTask().execute();
        }
    }

    public void stopMonitoring(){
        if (started) {
            started = false;
            hideNotification();
            if (client != null) {
                try {
                    client.getSensorManager().unregisterAllListeners();
                } catch (BandIOException e) {
                    Log.e(TAG, "Error while unregistering listeners",  e);
                }
            }
        }
    }

    private void displayNotification(){
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle(getString(R.string.notification_band_title))
                        .setContentText(getString(R.string.notification_band_content))
                        .setAutoCancel(false)
                        .setOngoing(true);
//                        .setProgress(0, 0, false);

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(538, mBuilder.build());
    }

    private void hideNotification(){
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.cancel(538);
    }

    private class MonitoringTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                if (getConnectedBandClient()) {
                    Log.d(TAG, "Band is connected.");

                    client.getSensorManager().registerAccelerometerEventListener(mAccelerometerEventListener, SampleRate.MS128);
//                    client.getSensorManager().registerCaloriesEventListener(mCaloriesEventListener);
                    client.getSensorManager().registerGyroscopeEventListener(mGyroscopeEventListener, SampleRate.MS128);

                    if (client.getSensorManager().getCurrentHeartRateConsent() == UserConsent.GRANTED) {
                        Log.d(TAG, "Heart rate is granted");
                        client.getSensorManager().registerHeartRateEventListener(mHeartRateEventListener);
                    } else if(activity != null){
                        Log.d(TAG, "Heart rate is not granted.");
                        client.getSensorManager().requestHeartRateConsent(activity, mHeartRateConsentListener);
                    }

//                    client.getSensorManager().registerDistanceEventListener(mDistanceEventListener);
//                    client.getSensorManager().registerSkinTemperatureEventListener(mSkinTemperatureEventListener);
//                    client.getSensorManager().registerUVEventListener(mUVEventListener);

                } else {
                    Log.d(TAG, "Band isn't connected. Please make sure bluetooth is on and the band is in range.");
                }
            } catch (BandException e) {
                String exceptionMessage = "";
                switch (e.getErrorType()) {
                    case UNSUPPORTED_SDK_VERSION_ERROR:
                        exceptionMessage = "Microsoft Health BandService doesn't support your SDK Version. Please update to latest SDK.";
                        break;
                    case SERVICE_ERROR:
                        exceptionMessage = "Microsoft Health BandService is not available. Please make sure Microsoft Health is installed and that you have the correct permissions.";
                        break;
                    default:
                        exceptionMessage = "Unknown error occured: " + e.getMessage();
                        break;
                }
                Log.d(TAG, exceptionMessage);

            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            }
            return null;
        }
    }

    private BandAccelerometerEventListener mAccelerometerEventListener = new BandAccelerometerEventListener() {
        @Override
        public void onBandAccelerometerChanged(final BandAccelerometerEvent event) {
            if (event != null) {
//                Log.d(TAG, String.format("Acceleration X = %.3fAcceleration Y = %.3fAcceleration Z = %.3f", event.getAccelerationX(),
//                        event.getAccelerationY(), event.getAccelerationZ()));
                Point point = Point.measurement("acceleration")
                        .time(event.getTimestamp(), TimeUnit.MILLISECONDS)
                        .field("x", event.getAccelerationX())
                        .field("y", event.getAccelerationY())
                        .field("z", event.getAccelerationZ())
                        .tag("username", userProfile.getScreenName())
                        .tag("device", "msband")
                        .build();
                influxDB.write(dbName, "default", point);
            }
        }
    };

//    private BandCaloriesEventListener mCaloriesEventListener = new BandCaloriesEventListener() {
//        @Override
//        public void onBandCaloriesChanged(final BandCaloriesEvent event) {
//            if (event != null) {
//                Log.d(TAG, String.format("Calories= %d", event.getCalories()));
//            }
//        }
//    };
//
//    private BandDistanceEventListener mDistanceEventListener = new BandDistanceEventListener() {
//        @Override
//        public void onBandDistanceChanged(final BandDistanceEvent event) {
//            if (event != null) {
//                Log.d(TAG, String.format("Pace = %.2fSpeed = %.2fTotal distance = %dMotion type = %s", event.getPace(), event.getSpeed(), event.getTotalDistance(), event.getMotionType().toString()));
//            }
//        }
//    };

    private BandGyroscopeEventListener mGyroscopeEventListener = new BandGyroscopeEventListener() {
        @Override
        public void onBandGyroscopeChanged(final BandGyroscopeEvent event) {
            if (event != null) {
//                Log.d(TAG, String.format("Angular velocity X = %.3fAngular velocity Y = %.3fAngular velocity Z = %.3f", event.getAngularVelocityX(), event.getAngularVelocityY(), event.getAngularVelocityZ()));
                Point point = Point.measurement("angular_velocity")
                        .time(event.getTimestamp(), TimeUnit.MILLISECONDS)
                        .field("x", event.getAngularVelocityX())
                        .field("y", event.getAngularVelocityY())
                        .field("z", event.getAngularVelocityZ())
                        .tag("username", userProfile.getScreenName())
                        .tag("device", "msband")
                        .build();
                influxDB.write(dbName, "default", point);
            }
        }
    };

    private BandHeartRateEventListener mHeartRateEventListener = new BandHeartRateEventListener() {
        @Override
        public void onBandHeartRateChanged(final BandHeartRateEvent event) {
            if (event != null) {
//                Log.d(TAG, String.format("Heart rate = %d Hz", event.getHeartRate()));
                Point point = Point.measurement("heartrate")
                        .time(event.getTimestamp(), TimeUnit.MILLISECONDS)
                        .field("rate", event.getHeartRate())
                        .field("quality", event.getQuality().ordinal())
                        .tag("username", userProfile.getScreenName())
                        .tag("device", "msband")
                        .build();
                influxDB.write(dbName, "default", point);
            }
        }
    };

//    private BandSkinTemperatureEventListener mSkinTemperatureEventListener = new BandSkinTemperatureEventListener() {
//        @Override
//        public void onBandSkinTemperatureChanged(final BandSkinTemperatureEvent event) {
//            if (event != null) {
//                Log.d(TAG, String.format("Skin Temperature = %.3f deg", event.getTemperature()));
//            }
//        }
//    };
//
//    private BandUVEventListener mUVEventListener = new BandUVEventListener() {
//        @Override
//        public void onBandUVChanged(final BandUVEvent event) {
//            if (event != null) {
//                Log.d(TAG, String.format("UV index level = %s", event.getUVIndexLevel().toString()));
//            }
//        }
//    };

    private boolean getConnectedBandClient() throws InterruptedException, BandException {
        if (client == null) {
            BandInfo[] devices = BandClientManager.getInstance().getPairedBands();
            if (devices.length == 0) {
                Log.d(TAG, "Band isn't paired with your phone.");
                return false;
            }
            client = BandClientManager.getInstance().create(getBaseContext(), devices[0]);

            //client.getSensorManager().getCurrentHeartRateConsent();
        } else if (ConnectionState.CONNECTED == client.getConnectionState()) {
            return true;
        }

        Log.d(TAG, "Band is connecting...");
        return ConnectionState.CONNECTED == client.connect().await();
    }


    private HeartRateConsentListener mHeartRateConsentListener = new HeartRateConsentListener() {
        @Override
        public void userAccepted(boolean b) {
            // handle user's heart rate consent decision
            if (b == true) {
                // Consent has been given, start HR sensor event listener
                //startHRListener();
            } else {
                // Consent hasn't been given
                Log.d(TAG, String.valueOf(b));
            }
        }
    };


}
