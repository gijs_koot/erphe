package nl.tno.erphe.wpxebuddy.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * This class describes the general functionality of a data access object (DAO).
 *
 * @author wabeketr
 */
public abstract class AbstractDAO {

    /**
     * @see android.database.sqlite.SQLiteDatabase
     */
    protected SQLiteDatabase database;

    /**
     * @see nl.tno.erphe.wpxebuddy.database.MySQLiteHelper
     */
    protected MySQLiteHelper dbHelper;

    /**
     * Default constructor that initializes a DAO.
     *
     * @param context
     */
    public AbstractDAO(final Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    /**
     * This function opens a database connection and is necessary for communicating with the
     * database.
     *
     * @throws android.database.SQLException
     */
    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    /**
     * This function closes the database function and should be called after interacting with the
     * database.
     */
    public void close() {
        dbHelper.close();
    }


}
