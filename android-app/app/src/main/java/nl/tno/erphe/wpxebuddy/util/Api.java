package nl.tno.erphe.wpxebuddy.util;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.json.JSONObject;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Thymen Wabeke on 11-6-2015.
 */
public class Api {

    private static final String TAG = "Api";

    public static final String BASE_URL = "http://144.76.75.238/erprapid";
    private static final int TIMEOUT = 15000;
    private int lastResponseCode;

    public int getLastResponseCode() {
        return lastResponseCode;
    }

    public String doGet(String url, String authentication)  throws Exception {
        Log.d(TAG, "doGet( " + url + ", " + authentication + " )");
        String result = null;
        HttpURLConnection urlConnection = null;
        try {

            URL requestedUrl = new URL(url);
            urlConnection = (HttpURLConnection) requestedUrl.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setConnectTimeout(TIMEOUT);
            urlConnection.setReadTimeout(TIMEOUT);
            urlConnection.setRequestProperty("Authentication", authentication);
            urlConnection.setDoInput(true);

            lastResponseCode = urlConnection.getResponseCode();

            result = IOUtil.readFully(urlConnection.getInputStream());

            Log.d(TAG, "GET " + requestedUrl.toString() + " = " + lastResponseCode);

        } catch(Exception ex) {
            result = ex.toString();
            Log.e(TAG, "Exception was thrown while GET " + url.toString(), ex);
        } finally {
            if(urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return result;
    }

    public String doGet(String url)  throws Exception {
        return doGet(url,"");
    }

    public JSONObject doGetJson(String url, String authentication)  throws Exception {
        return new JSONObject(doGet(url,authentication));
    }

    public JSONObject doGetJson(String url)  throws Exception {
        return new JSONObject(doGet(url,""));
    }

    private String doPostOrPut(String url, String method, JSONObject data, String authentication)  throws Exception {
        String result = null;

        HttpURLConnection urlConnection = null;
        try {

            URL requestedUrl = new URL(url);
            urlConnection = (HttpURLConnection) requestedUrl.openConnection();
            urlConnection.setRequestMethod(method);
            urlConnection.setConnectTimeout(TIMEOUT);
            urlConnection.setReadTimeout(TIMEOUT);
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Authentication", authentication);
            urlConnection.setRequestProperty("content-type", "application/json; UTF-8");

            OutputStream os = urlConnection.getOutputStream();
            os.write(data.toString().getBytes("UTF-8"));
            os.close();

            lastResponseCode = urlConnection.getResponseCode();
            result = IOUtil.readFully(urlConnection.getInputStream());

            Log.d(TAG, method + " " + authentication + "@" + requestedUrl.toString() + " = " + lastResponseCode);
        } catch (Exception ex) {
            result = ex.toString();
            Log.e(TAG, "Exception was thrown while " + method + " " + url.toString() + " " + lastResponseCode + " - " + data.toString(), ex);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return result;
    }

    public String doPost(String url, JSONObject data, String authentication)  throws Exception {
        return doPostOrPut(url,"POST",data,authentication);
    }

    public JSONObject doPostJson(String url, JSONObject data, String authentication)  throws Exception {
        return new JSONObject(doPost(url, data, authentication));
    }

    public JSONObject doPostJson(String url, JSONObject data)  throws Exception {
        return new JSONObject(doPost(url,data,""));
    }

    public String doPut(String url, JSONObject data, String authentication)  throws Exception {
        return doPostOrPut(url,"PUT",data,authentication);
    }

    public JSONObject doPutJson(String url, JSONObject data, String authentication)  throws Exception {
        return new JSONObject(doPut(url, data, authentication));
    }

    public static boolean isOnline(final Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}