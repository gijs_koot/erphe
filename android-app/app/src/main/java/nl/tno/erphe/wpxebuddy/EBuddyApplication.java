package nl.tno.erphe.wpxebuddy;

import android.app.Application;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

/**
 * Created by thymen on 14/07/15.
 */
@ReportsCrashes(
//        formKey = "", // This is required for backward compatibility but not used
        formUri = "http://thymen.com/wifi/log.php",
        mode = ReportingInteractionMode.SILENT
)
public class EBuddyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // The following line triggers the initialization of ACRA
        ACRA.init(this);
    }

}