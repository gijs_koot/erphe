package nl.tno.erphe.wpxebuddy.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Thymen Wabeke on 11-6-2015.
 */
public class UserProfile {

    public static final String PREF_SCREEN_NAME = "screen_name";
    public static final String PREF_AUTHENTICATION = "authentication";
    public static final String PREF_FOREIGN_ID = "uid";
    public static final String PREF_IS_AUTHENTICATED = "is_authenticated";

    SharedPreferences sharedPrefs;

    public UserProfile(final Context context){
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean isAuthenticated(){
        return sharedPrefs.getBoolean(PREF_IS_AUTHENTICATED, false);
    }

    public void login(String screenName, String authentication, String foreignId){
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(PREF_FOREIGN_ID, foreignId);
        editor.putString(PREF_SCREEN_NAME, screenName);
        editor.putString(PREF_AUTHENTICATION, authentication);
        editor.putBoolean(PREF_IS_AUTHENTICATED, true);
        editor.commit();
    }

    public String getScreenName(){
        return sharedPrefs.getString(PREF_SCREEN_NAME, null);
    }

    public String getAuthentication() {
        return sharedPrefs.getString(PREF_AUTHENTICATION, null);
    }

    public String getForeignId(){
        return sharedPrefs.getString(PREF_FOREIGN_ID, null);
    }


}


