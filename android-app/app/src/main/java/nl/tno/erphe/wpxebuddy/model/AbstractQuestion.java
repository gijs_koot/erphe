package nl.tno.erphe.wpxebuddy.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Thymen Wabeke on 19-5-2015.
 */
public abstract class AbstractQuestion implements Serializable {

    protected long id = -1L;
    protected String foreignId = "";
    protected String title;
    protected String description;
    protected Answer answer;

    public final static String JSON_TAG_FOREIGN_ID = "_id";
    public final static String JSON_TAG_TYPE = "type";
    public final static String JSON_TAG_TITLE = "title";
    public final static String JSON_TAG_DESCRIPTION = "description";
    public final static String JSON_TAG_OPTIONS = "options";
    public final static String JSON_TAG_VALUE = "value";
    public final static String JSON_TAG_KEY = "key";
    public final static String JSON_TAG_LABEL = "label";
    public final static String JSON_TAG_ANSWER = "answer";


    public AbstractQuestion(long id, String foreignId, String title, String description, Answer answer) {
        this.foreignId = foreignId;
        this.id = id;
        this.title = title;
        this.description = description;
        this.answer = answer;
    }

    public AbstractQuestion(long id) {
        this(id,null,null,null,null);
    }

    public AbstractQuestion(String foreignId, String title, String description) {
        this(-1L,foreignId,title,description,null);
    }

    public AbstractQuestion(long id, JSONObject jsonQuestion) throws JSONException {
        this.id = id;
        this.answer = null;
        fromJSON(jsonQuestion);
    }

    public AbstractQuestion(JSONObject jsonQuestion) throws JSONException {
        this(-1L,jsonQuestion);
    }

    public abstract JSONObject toJSON() throws JSONException;
    public abstract void fromJSON(JSONObject jsonQuestion) throws JSONException;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getForeignId() {
        return foreignId;
    }

    public void setForeignId(String foreignId) {
        this.foreignId = foreignId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Answer getAnswer() {
        return answer;
    }

    public boolean hasAnswer(){
        return (answer != null && answer.getResponse() != null);
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public abstract int getQuestionType();

    @Override
    public String toString() {
        return "AbstractQuestion{" +
                "id=" + id +
                ", foreignId=" + foreignId +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", answer=" + answer +
                '}';
    }
}
