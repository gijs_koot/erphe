package nl.tno.erphe.wpxebuddy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import nl.tno.erphe.wpxebuddy.database.QuestionDAO;
import nl.tno.erphe.wpxebuddy.model.AbstractQuestion;
import nl.tno.erphe.wpxebuddy.service.GetDataService;
import nl.tno.erphe.wpxebuddy.util.Api;
import nl.tno.erphe.wpxebuddy.util.UserProfile;

/**
 * This abstract class declares the functionality shared by all classes specifying a question
 * fragment. Each instantiation of a question fragment represents the UI of a single question.
 *
 * @author thymen
 */
public abstract class AbstractQuestionFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "AbstractQuestionFrgmnt";

    public static final String HAS_NEXT_QUESTION_TAG = "hasNextQuestion";
    public static final String HAS_PREVIOUS_QUESTION_TAG = "hasPreviousQuestion";
    public static final String QUESTION_TYPE_TAG = "questionType";
    public static final String QUESTION_OBJECT_TAG = "questionObject";

    protected TextView titleTextView;
    protected TextView descriptionTextView;
    protected ViewPager mPager;
    protected Button nextButton;
    protected Button previousButton;

    protected QuestionDAO questionDAO;
    protected Api api;
    protected UserProfile userProfile;

    protected boolean hasNextQuestion;
    protected boolean hasPreviousQuestion;


    public AbstractQuestionFragment() {
    }


    /**
     * Initialize the question object the fragment is about.
     *
     * The question object is stored in the app's database. The fragment should use a Bundle or
     * IntentExtra to know which question it should load.
     */
    protected abstract void initQuestionObject();

    /**
     * Update the question object in the database.
     */
    protected abstract void updateQuestionObject();


    /**
     * A Bundle should be attached to the Intent that opens a question fragment. This function
     * loads the Bundle and processes it.
     */
    protected void processBundle(){
        Bundle bundle = getArguments();
        hasNextQuestion = bundle.getBoolean(HAS_NEXT_QUESTION_TAG);
        hasPreviousQuestion = bundle.getBoolean(HAS_PREVIOUS_QUESTION_TAG);
    }

    /**
     * This function shows a TOAST that warns the user if the device has no internet connection.
     *
     * @param context The app's context
     */
    protected void warnIfOffline(final Context context){
        if(!Api.isOnline(context)){
            Toast.makeText(context, context.getString(R.string.question_no_internet),
                    Toast.LENGTH_LONG).show();
        }
    }

    /**
     * This function processes an answer that has been given to a question.
     *
     * The answer processing consists of two steps:
     * <ol>
     *     <li>The question object is updated in the database;</li>
     *     <li>The answer is sent to the server.</li>
     * </ol>
     *
     * @param question The question to which an answer has been given
     */
    protected void handleAnswer(final AbstractQuestion question) {
        questionDAO.open();
        questionDAO.updateQuestion(question);
        questionDAO.close();
        JSONObject jsonAnswer = new JSONObject();
        try {
            jsonAnswer.accumulate("answer", question.getAnswer().toJSON());
        } catch (JSONException e) {
            Log.e(TAG, "Could not prepare JSON object", e);
        }
        String url = String.format("%s/questions/%s", Api.BASE_URL, question.getForeignId());
        sendAnswer(url, jsonAnswer);
    }

    /**
     * This function sends an answer to the webservice. The communication with the webservice is
     * performed in separate thread.
     *
     * @param url The URL were the answer should be put
     * @param jsonAnswer The answer that need to be put (in JSON format)
     */
    protected void sendAnswer(final String url, final JSONObject jsonAnswer) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    api.doPut(url, jsonAnswer, userProfile.getAuthentication());
                } catch (Exception e) {
                    Log.e(TAG, "Could not send answer to webservice");
                }
            }
        };
        new Thread(runnable).start();
    }

    /**
     * This function loads the header of the question UI.
     *
     * @param rootView The root UI of the question's fragment
     * @param question The question object itself
     */
    protected void loadQuestionHeader(final View rootView, final AbstractQuestion question) {
        titleTextView = (TextView) rootView.findViewById(R.id.question_title);
        descriptionTextView = (TextView) rootView.findViewById(R.id.question_description);

        titleTextView.setText(question.getTitle());

        if (question.getDescription() != null && !question.getDescription().isEmpty()) {
            descriptionTextView.setText(question.getDescription());
        } else {
            descriptionTextView.setVisibility(View.GONE);
        }
    }

    /**
     * This function loads the footer of the question UI.
     *
     * @param rootView The root UI of the question's fragment
     * @param question The question object itself
     */
    protected void loadQuestionFooter(final View rootView, final AbstractQuestion question) {
        nextButton = (Button) rootView.findViewById(R.id.button_next);
        previousButton = (Button) rootView.findViewById(R.id.button_previous);
        nextButton.setOnClickListener(this);
        previousButton.setOnClickListener(this);

        nextButton.setEnabled(true);

        if(!hasNextQuestion){
            nextButton.setText("Opslaan!");
        }

        if(!hasPreviousQuestion){
            previousButton.setVisibility(View.INVISIBLE);
        }

    }

    protected void onClick(View v, final AbstractQuestion question) {
        switch (v.getId()) {

            case R.id.button_next:

                if (question.hasAnswer()) {
                    handleAnswer(question);
                }

                if(hasNextQuestion){
                    ((QuestionnaireActivity) getActivity()).moveNext();
                } else{
                    Intent mIntent = new Intent(getActivity(), GetDataService.class);
                    getActivity().startService(mIntent);

                    mIntent = new Intent(getActivity(), QuestionnaireListActivity.class);
                    getActivity().startActivity(mIntent);
                }

                break;

            case R.id.button_previous:

                if (hasPreviousQuestion && question.hasAnswer()) {
                    handleAnswer(question);
                }
                ((QuestionnaireActivity) getActivity()).movePrevious();

                break;

            default:
                break;
        }
    }

}
