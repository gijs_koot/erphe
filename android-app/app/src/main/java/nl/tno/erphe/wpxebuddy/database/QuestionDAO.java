package nl.tno.erphe.wpxebuddy.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import nl.tno.erphe.wpxebuddy.model.AbstractQuestion;
import nl.tno.erphe.wpxebuddy.model.ListQuestion;
import nl.tno.erphe.wpxebuddy.model.QuestionTypes;
import nl.tno.erphe.wpxebuddy.model.Questionnaire;
import nl.tno.erphe.wpxebuddy.model.ScaleQuestion;
import nl.tno.erphe.wpxebuddy.model.TextQuestion;

/**
 * Created by Thymen Wabeke on 29-5-2015.
 */
public class QuestionDAO extends AbstractDAO {

    public static final String TAG = "QuestionDAO";

    /**
     * Default constructor that initializes a DAO.
     *
     * @param context
     */
    public QuestionDAO(final Context context) {
        super(context);
    }

    public long insertQuestion(AbstractQuestion question, Questionnaire questionnaire) {
        if (question.getForeignId() == null || question.getForeignId().isEmpty()) {
            Log.w(TAG, "insertQuestion() failed, cause no foreign id was set");
            return -1L;
        }
        if (questionnaire.getId() < -1L) {
            Log.w(TAG, "insertQuestion() failed, cause no questionnaire id was set");
            return -1L;
        }
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_QUESTIONNAIRE_ID, questionnaire.getId());
        values.put(MySQLiteHelper.COLUMN_FOREIGN_ID, question.getForeignId());
        values.put(MySQLiteHelper.COLUMN_TYPE, question.getQuestionType());
        try {
            values.put(MySQLiteHelper.COLUMN_JSON_REPRESENTATION, question.toJSON().toString());
        } catch (JSONException e) {
            Log.e(TAG, "insertQuestion() failed, cause question could not be converted to JSON", e);
            return -1L;
        }
        return database.insert(MySQLiteHelper.TABLE_QUESTIONS, null, values);
    }

    public boolean updateQuestion(AbstractQuestion question) {
        long id = question.getId();
        if (id == -1L) {
            Log.w(TAG, "updateQuestion() failed, cause no ID was set.");
            return false;
        }

        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_FOREIGN_ID, question.getForeignId());
        values.put(MySQLiteHelper.COLUMN_TYPE, question.getQuestionType());
        try {
            values.put(MySQLiteHelper.COLUMN_JSON_REPRESENTATION, question.toJSON().toString());
        } catch (JSONException e) {
            Log.e(TAG, "updateQuestion() failed, cause question could not be converted to JSON", e);
            return false;
        }

        int numRows = database.update(MySQLiteHelper.TABLE_QUESTIONS, values, MySQLiteHelper.COLUMN_ID
                + " = " + id, null);

        if (numRows != 1) {
            Log.i(TAG, "updateQuestion() for Question.getId() = " + question.getId() +
                    " affected " + numRows + " row(s) (where 1 row was expected)");
            return false;
        }
        return true;
    }

    public boolean updateQuestion(AbstractQuestion question, Questionnaire questionnaire) {
        long id = question.getId();
        if (id == -1L) {
            Log.w(TAG, "updateQuestion() failed, cause no ID was set.");
            return false;
        }
        if (questionnaire.getId() < -1L) {
            Log.w(TAG, "insertQuestion() failed, cause no questionnaire id was set");
            return false;
        }

        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_FOREIGN_ID, question.getForeignId());
        values.put(MySQLiteHelper.COLUMN_QUESTIONNAIRE_ID, questionnaire.getId());
        values.put(MySQLiteHelper.COLUMN_TYPE, question.getQuestionType());
        try {
            values.put(MySQLiteHelper.COLUMN_JSON_REPRESENTATION, question.toJSON().toString());
        } catch (JSONException e) {
            Log.e(TAG, "updateQuestion() failed, cause question could not be converted to JSON", e);
            return false;
        }

        int numRows = database.update(MySQLiteHelper.TABLE_QUESTIONS, values, MySQLiteHelper.COLUMN_ID
                + " = " + id, null);

        if (numRows != 1) {
            Log.i(TAG, "updateQuestion() for Question.getId() = " + question.getId() +
                    " affected " + numRows + " row(s) (where 1 row was expected)");
            return false;
        }
        return true;
    }

    public AbstractQuestion getQuestionByForeignId(String foreignId) {
        List<AbstractQuestion> questions = new ArrayList<>();

        Cursor cursor = database.rawQuery("SELECT * FROM " + MySQLiteHelper.TABLE_QUESTIONS
                        + " WHERE " + MySQLiteHelper.COLUMN_FOREIGN_ID + " = ?;",
                new String[]{String.valueOf(foreignId)});

        if(cursor == null || cursor.getCount() == 0) {
            Log.d(TAG, "getQuestionByForeignId( foreignId = " + foreignId
                    + " ) returned no results") ;
            return null;
        }
        if(cursor.getCount() != 1) {
            Log.d(TAG, "getQuestionByForeignId( foreignId = " + foreignId + " ) returned "
                    + cursor.getCount() + " row(s) (where 1 row was expected)");
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            AbstractQuestion question = cursorToQuestion(cursor);
            questions.add(question);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();

        return questions.get(questions.size()-1);
    }

    public List<AbstractQuestion> getQuestionsByQuestionnaire(Questionnaire questionnaire) {
        List<AbstractQuestion> questions = new ArrayList<>();
        long questionnaireID = questionnaire.getId();
        if (questionnaireID < 0L) {
            Log.w(TAG, "getQuestionsByQuestionnaire() failed, cause no questionnaire ID was set.");
            return null;
        }

        Cursor cursor = database.rawQuery("SELECT * FROM " + MySQLiteHelper.TABLE_QUESTIONS
                        + " WHERE " + MySQLiteHelper.COLUMN_QUESTIONNAIRE_ID + " = ?;",
                new String[]{String.valueOf(questionnaireID)});

        if(cursor == null || cursor.getCount() == 0) {
            Log.d(TAG, "getQuestionsByQuestionnaire( questionnaireID = " + questionnaireID
                    + " ) returned no results");
            return null;
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            AbstractQuestion question = cursorToQuestion(cursor);
            questions.add(question);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();

        return questions;
    }

    private AbstractQuestion cursorToQuestion(Cursor cursor) {
        int questionType = cursor.getInt(cursor.getColumnIndex(MySQLiteHelper.COLUMN_TYPE));
        JSONObject jsonRepresentation;
        try {
            jsonRepresentation = new JSONObject(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_JSON_REPRESENTATION)));
        } catch (JSONException e) {
            Log.w(TAG, "Could not convert cursorToQuestion", e);
            return null;
        }

        if (questionType == QuestionTypes.LIST) {
            try {
                return new ListQuestion(
                        cursor.getLong(cursor.getColumnIndex(MySQLiteHelper.COLUMN_ID)),
                        jsonRepresentation);
            } catch (JSONException e) {
                Log.w(TAG, "Could not convert cursorToQuestion", e);
                return null;
            }
        } else if (questionType == QuestionTypes.SCALE) {
            try {
                return new ScaleQuestion(
                        cursor.getLong(cursor.getColumnIndex(MySQLiteHelper.COLUMN_ID)),
                        jsonRepresentation);
            } catch (JSONException e) {
                Log.w(TAG, "Could not convert cursorToQuestion", e);
                return null;
            }
        } else if (questionType == QuestionTypes.TEXT) {
            try {
                return new TextQuestion(
                        cursor.getLong(cursor.getColumnIndex(MySQLiteHelper.COLUMN_ID)),
                        jsonRepresentation);
            } catch (JSONException e) {
                Log.w(TAG, "Could not convert cursorToQuestion", e);
                return null;
            }
        }
        return null;

    }
}
