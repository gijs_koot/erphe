package nl.tno.erphe.wpxebuddy;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import nl.tno.erphe.wpxebuddy.database.QuestionDAO;
import nl.tno.erphe.wpxebuddy.model.Answer;
import nl.tno.erphe.wpxebuddy.model.TextQuestion;
import nl.tno.erphe.wpxebuddy.util.Api;
import nl.tno.erphe.wpxebuddy.util.UserProfile;

public class TextQuestionFragment extends AbstractQuestionFragment {

    public static final String TAG = "TextQuestionFragment";

    private View rootView;
    private TextQuestion question;
    private EditText editText;

    public TextQuestionFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        questionDAO = new QuestionDAO(getActivity());
        api = new Api();
        userProfile = new UserProfile(getActivity());

        processBundle();
        initQuestionObject();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView() called");
        rootView = inflater.inflate(R.layout.fragment_question_text, container, false);
        mPager = ((QuestionnaireActivity) getActivity()).getViewPager();

        loadQuestionHeader(rootView, question);
        loadQuestionFooter(rootView, question);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateQuestionObject();
        loadTextInput();
        warnIfOffline(getActivity());
    }

    @Override
    protected void initQuestionObject() {
        question = (TextQuestion) getArguments().getSerializable(QUESTION_OBJECT_TAG);
        updateQuestionObject();
    }

    @Override
    protected void updateQuestionObject() {
        questionDAO.open();
        question = (TextQuestion) questionDAO.getQuestionByForeignId(question.getForeignId());
        questionDAO.close();
    }

    private void loadTextInput() {
        Log.d(TAG, "loadTextInput() called");
        editText = (EditText) rootView.findViewById(R.id.user_input);

        if (question.hasAnswer()) {
            editText.setText(question.getAnswer().getResponse());
        }

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.button_next || v.getId() == R.id.button_previous){
            String textInput = editText.getText().toString();
            if (textInput != null && !textInput.isEmpty()) {
                question.setAnswer(new Answer(textInput));
            }
            InputMethodManager inputManager = (InputMethodManager)
                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
        onClick(v, question);
    }
}
