package nl.tno.erphe.wpxebuddy.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import nl.tno.erphe.wpxebuddy.database.QuestionDAO;
import nl.tno.erphe.wpxebuddy.database.QuestionnaireDAO;
import nl.tno.erphe.wpxebuddy.model.AbstractQuestion;
import nl.tno.erphe.wpxebuddy.model.ListQuestion;
import nl.tno.erphe.wpxebuddy.model.QuestionTypes;
import nl.tno.erphe.wpxebuddy.model.Questionnaire;
import nl.tno.erphe.wpxebuddy.model.ScaleQuestion;
import nl.tno.erphe.wpxebuddy.model.TextQuestion;
import nl.tno.erphe.wpxebuddy.receiver.StartSyncReceiver;
import nl.tno.erphe.wpxebuddy.util.Api;
import nl.tno.erphe.wpxebuddy.util.ISO8601;
import nl.tno.erphe.wpxebuddy.util.UserProfile;

/**
 * This service is used to update the device's model with the model stored on the server/webservice.
 * The model includes the user's questionnaires, questions within questionnaires, answers to questions.
 * The service runs in a separate thread and thus not blocks the UI.
 */
public class GetDataService extends IntentService {

    public static final String TAG = "GetDataService";
    public static final String SYNC_FINISHED = "nl.tno.erphe.wpxebuddy.SycnFinished";
    private UserProfile userProfile;
    private Api api;

    public GetDataService() {
        super("GetDataService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(TAG, "Starting service @ " + SystemClock.elapsedRealtime());

        // Initialize the user profile
        userProfile = new UserProfile(this);
        api = new Api();
        // Check if the user is authenticated and the device has an internet connection
        if (userProfile.isAuthenticated() && Api.isOnline(this)) {
            // Retrieve the model from the webservice and save it for later use
            JSONObject serverResponse = performQuestionnairesRequest();
//            Log.d(TAG, serverResponse.toString());
            // Process the model/JSONObject that was retrieved
            List<Questionnaire> questionnaires = getQuestionnaires(serverResponse);
            // Update the app's model
            for(Questionnaire questionnaire : questionnaires){
                updateOrInsertQuestionnaire(questionnaire);
            }
        }

        // The model has been updated so new questionnaires may be available. Create a
        // NotificationService to notify the users for new questionnaires.
        Intent nIntent = new Intent(this, NotificationService.class);
        startService(nIntent);

        Intent iFinished = new Intent(SYNC_FINISHED);
        sendBroadcast(iFinished);
        Log.i(TAG, "Completed service @ " + SystemClock.elapsedRealtime());

        // Release the wake lock (if any)
        StartSyncReceiver.completeWakefulIntent(intent);
    }

    /**
     * Retrieve the list of questionnaires from the webservice.
     *
     * @return A JSONObject containing the list of questionnaires
     */
    private JSONObject performQuestionnairesRequest(){
        // Prepare the URL
        String url = String.format("%s/users/%s/questionnaires", Api.BASE_URL, userProfile.getScreenName());
        JSONObject jsonObject;

        // Perform the request
        try {
            jsonObject = api.doGetJson(url,userProfile.getAuthentication());
        } catch (Exception e) {
            Log.d(TAG, "performQuestionnairesRequest() failed", e);
            return new JSONObject();
        }

        // Check whether the request was successful
        if(api.getLastResponseCode() == HttpURLConnection.HTTP_OK){
            return jsonObject;
        } else{
            Log.d(TAG, "performQuestionnairesRequest() failed; status code = " + api.getLastResponseCode());
            return new JSONObject();
        }
    }

    /**
     * Process a JSONObject containing a list of questionnaires.
     *
     * @param jsonObject A list of questionnaires retrieved from the webservice
     * @return A list of Questionnaire's that complies with the app's model
     */
    private List<Questionnaire> getQuestionnaires(JSONObject jsonObject){
        List<Questionnaire> output = new ArrayList<>();
        try {
            // Get the JSONArray containing the questionnaires
            JSONArray jsonArray = jsonObject.getJSONArray("questionnaires");

            Log.d(TAG, "#" + jsonArray.length() + " questionnaires were downloaded for this user");

            // Loop the the questionnaires in jsonArray
            for (int i = 0; i < jsonArray.length(); ++i) {
                JSONObject jsonQuestionnaire = jsonArray.getJSONObject(i);

                String id = jsonQuestionnaire.getString(Questionnaire.JSON_TAG_FOREIGN_ID);
                long dueTimestamp = ISO8601.toTimesamp(jsonQuestionnaire.getString(Questionnaire.JSON_TAG_DUE));
//                dueTimestamp -= (1000L*3600L*2L); // TODO: more elegant fix to get the timezones in sync
                boolean completed = jsonQuestionnaire.getBoolean(Questionnaire.JSON_TAG_COMPLETED);
                double completedFraction = jsonQuestionnaire.getDouble(Questionnaire.JSON_TAG_COMPLETED_FRACTION);
                String type = jsonQuestionnaire.getString(Questionnaire.JSON_TAG_TYPE);
                String title = jsonQuestionnaire.getString(Questionnaire.JSON_TAG_TITLE);

                Log.d(TAG, "Questionnaire #" + i + ": foreignId = " + id + "; dueTimestamp = "
                        + jsonQuestionnaire.getString(Questionnaire.JSON_TAG_DUE) + ";  completedFraction = "
                        + completedFraction + "; title = " + title);

                // Get the JSONArray containing the questions for this questionnaires
                JSONArray jsonQuestions = jsonQuestionnaire.getJSONArray("questions");
                // Parse the JSONArray containing the questions
                List<AbstractQuestion> questions = getQuestions(jsonQuestions);

                // Add the parsed questionnaire to the output
                output.add(new Questionnaire(id,dueTimestamp,completed, completedFraction,type,title, questions));
            }
        } catch (Exception e) {
            Log.e(TAG, "An error occurred while parsing the data from the webservice", e);
        }
        return output;
    }

    /**
     *
     * @param jsonQuestions
     * @return
     */
    private List<AbstractQuestion> getQuestions(JSONArray jsonQuestions){
        List<AbstractQuestion> output = new ArrayList<>();
        try {
            for (int k = 0; k < jsonQuestions.length(); ++k) {
                JSONObject jsonQ = jsonQuestions.getJSONObject(k);
                String type = jsonQ.getString(AbstractQuestion.JSON_TAG_TYPE);
                if(type.equals(QuestionTypes.LIST_TAG)){
                    output.add(new ListQuestion(jsonQ));
                } else if(type.equals(QuestionTypes.SCALE_TAG)){
                    output.add(new ScaleQuestion(jsonQ));
                } else if (type.equals(QuestionTypes.TEXT_TAG)) {
                    output.add(new TextQuestion(jsonQ));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return output;
    }

    /**
     *
     * @param questionnaire
     */
    private void updateOrInsertQuestionnaire(Questionnaire questionnaire){
        long questionnaireId;

        QuestionnaireDAO questionnaireDAO = new QuestionnaireDAO(this);
        questionnaireDAO.open();
        Questionnaire tempQuestionnaire =
                questionnaireDAO.getQuestionnaireByForeignId(questionnaire.getForeignId());
        if(tempQuestionnaire != null){
            questionnaireId = tempQuestionnaire.getId();
            questionnaire.setId(questionnaireId);
            questionnaire.setIsUserNotified(tempQuestionnaire.isUserNotified());
            questionnaireDAO.updateQuestionnaire(questionnaire);
        } else{
            questionnaireId = questionnaireDAO.insertQuestionnaire(questionnaire);
            questionnaire.setId(questionnaireId);
        }
        questionnaireDAO.close();

        for(AbstractQuestion question : questionnaire.getQuestions()){
            updateOrInsertQuestion(question, questionnaire);
        }
    }

    /**
     *
     * @param question
     * @param questionnaire
     */
    private void updateOrInsertQuestion(AbstractQuestion question, Questionnaire questionnaire){
        QuestionDAO questionDAO = new QuestionDAO(this);
        questionDAO.open();

        AbstractQuestion tempQuestion =
                questionDAO.getQuestionByForeignId(question.getForeignId());
        if(tempQuestion != null){
            long questionId = tempQuestion.getId();
            question.setId(questionId);
            questionDAO.updateQuestion(question, questionnaire);
        } else{
            long questionId = questionDAO.insertQuestion(question,questionnaire);
            question.setId(questionId);
        }

        questionDAO.close();
    }


}
