package nl.tno.erphe.wpxebuddy;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import nl.tno.erphe.wpxebuddy.model.Questionnaire;

public class QuestionnaireAdapter extends ArrayAdapter<Questionnaire> {

    private List<Questionnaire> questionnaireList;

     private final static String TAG = "QuestionnaireAdapter";

    private final Context context;

    static class ViewHolder {
        public TextView firstLine;
        public TextView secondLine;
        public TextView time;
        public ImageView icon;
        public long date;
    }

    public QuestionnaireAdapter(Context context,
                                     List<Questionnaire> questionnaireList) {
        super(context, R.layout.item_row, questionnaireList);
        this.context = context;
        this.questionnaireList = questionnaireList;
        Resources res = context.getResources();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.item_row, parent, false);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.firstLine = (TextView) rowView
                    .findViewById(R.id.element_first_line);
            viewHolder.secondLine = (TextView) rowView
                    .findViewById(R.id.element_second_line);
            viewHolder.icon = (ImageView) rowView
                    .findViewById(R.id.element_icon);
            viewHolder.time = (TextView) rowView
                    .findViewById(R.id.element_time);
            viewHolder.date = 0L;
            rowView.setTag(viewHolder);
        }

        // fill data
        ViewHolder holder = (ViewHolder) rowView.getTag();
        Questionnaire q = null;
        SimpleDateFormat dateFormat = null;
        if (questionnaireList.size() >= position) {
            q = questionnaireList.get(position);
        }
        if (q != null) {
            if (q.isCompleted()) {
                holder.icon.setImageResource(R.drawable.ic_completed);
            } else {
                holder.icon.setImageResource(R.drawable.ic_uncompleted);
            }

            if (holder.firstLine != null) {
                holder.firstLine.setText(q.getTitle());
            }
            holder.date = q.getDueTimestamp();

            if (holder.time != null) {
                holder.date = q.getDueTimestamp();

                if (diff(holder.date, Calendar.DAY_OF_YEAR) == 0.0) {
                    // Today
                    dateFormat = new SimpleDateFormat("kk:mm",
                            Locale.getDefault());
                    holder.time.setText(dateFormat.format(holder.date));
                } else if (diff(holder.date, Calendar.DAY_OF_YEAR) == -1.0) {
                    // Yesterday
                    holder.time.setText("gisteren");
                } else if (diff(holder.date, Calendar.DAY_OF_YEAR) > -5.0) {
                    // Past five days
                    dateFormat = new SimpleDateFormat("EEEE",
                            Locale.getDefault());
                    holder.time.setText(dateFormat.format(holder.date));
                } else {
                    // Long ago
                    dateFormat = new SimpleDateFormat("dd-MM-yy",
                            Locale.getDefault());
                    holder.time.setText(dateFormat.format(holder.date));
                }
            }
            if (holder.secondLine != null) {
                holder.secondLine.setText("Voor " + String.valueOf((int) (q.getCompletedFraction()*100L)) + "% ingevuld");
            }
        }
        return rowView;
    }

    public static long diff(long dateToCompare, int field) {
        long fieldTime = getFieldInMillis(field);
        Calendar cal = Calendar.getInstance();
        long now = cal.getTimeInMillis();
//        Log.d(TAG, "NOW = " + now);
//        Log.d(TAG, "DUE = " + dateToCompare);
        return (dateToCompare / fieldTime - now / fieldTime);
    }

    private static final long getFieldInMillis(int field) {
        final Calendar cal = Calendar.getInstance();
        long now = cal.getTimeInMillis();
        cal.add(field, 1);
        long after = cal.getTimeInMillis();
        return after - now;
    }

    @Override
    public int getCount() {
        return questionnaireList != null ? questionnaireList.size() : 0;
    }

    @Override
    public Questionnaire getItem(int position) {
        return questionnaireList.get(position);
    }

    /**
     * Get a diff between two dates
     *
     * @param date1
     *            the oldest date
     * @param date2
     *            the newest date
     * @param timeUnit
     *            the unit in which you want the diff
     * @return the diff value, in the provided unit
     */
    public static long getDateDiff(long date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1;
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

}