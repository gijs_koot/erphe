package nl.tno.erphe.wpxebuddy.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Thymen Wabeke on 19-5-2015.
 */
public class ScaleQuestion extends AbstractQuestion implements Serializable {

    public final static int QUESTION_TYPE = QuestionTypes.SCALE;
    private String minLabel;
    private String maxLabel;

    private int minValue;
    private int maxValue;

    public final static String JSON_TAG_MAX = "max";
    public final static String JSON_TAG_MIN = "min";


    public ScaleQuestion(long id, String foreignID, String title, String description, Answer answer) {
        super(id, foreignID, title, description, answer);
    }

    public ScaleQuestion(String foreignID, String title, String description) {
        super(foreignID, title, description);
    }

    public ScaleQuestion(long id, JSONObject jsonQuestion) throws JSONException {
        super(id, jsonQuestion);
    }

    public ScaleQuestion(JSONObject jsonQuestion) throws JSONException {
        super(jsonQuestion);
    }

//    public ScaleQuestion(int id, JSONObject jsonQuestion) throws JSONException {
//        super(id, jsonQuestion);
//    }
//
//    public ScaleQuestion(int id, String title, String description) {
//        super(id, title, description);
//    }

    public String getMinLabel() {
        return minLabel;
    }

    public void setMinLabel(String minLabel) {
        this.minLabel = minLabel;
    }

    public String getMaxLabel() {
        return maxLabel;
    }

    public void setMaxLabel(String maxLabel) {
        this.maxLabel = maxLabel;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        JSONObject maxObject = new JSONObject();
        JSONObject minObject = new JSONObject();

        jsonObject.accumulate(JSON_TAG_FOREIGN_ID, foreignId);
        jsonObject.accumulate(JSON_TAG_TYPE, QUESTION_TYPE);
        jsonObject.accumulate(JSON_TAG_TITLE, title);
        jsonObject.accumulate(JSON_TAG_DESCRIPTION, description);

        maxObject.accumulate(JSON_TAG_LABEL, maxLabel);
        maxObject.accumulate(JSON_TAG_VALUE, maxValue);

        minObject.accumulate(JSON_TAG_LABEL, minLabel);
        minObject.accumulate(JSON_TAG_VALUE, minValue);

        jsonObject.accumulate(JSON_TAG_MAX, maxObject);
        jsonObject.accumulate(JSON_TAG_MIN, minObject);

        if(hasAnswer()){
            jsonObject.accumulate(JSON_TAG_ANSWER,answer);
        }

        return jsonObject;
    }

    @Override
    public void fromJSON(JSONObject jsonQuestion) throws JSONException {
        foreignId = jsonQuestion.getString(JSON_TAG_FOREIGN_ID);
        title = jsonQuestion.getString(JSON_TAG_TITLE);
        description = jsonQuestion.getString(JSON_TAG_DESCRIPTION);
        JSONObject maxObject = jsonQuestion.getJSONObject(JSON_TAG_MAX);
        JSONObject minObject = jsonQuestion.getJSONObject(JSON_TAG_MIN);
        maxLabel = maxObject.getString(JSON_TAG_LABEL);
        minLabel = minObject.getString(JSON_TAG_LABEL);
        maxValue = maxObject.getInt(JSON_TAG_VALUE);
        minValue = minObject.getInt(JSON_TAG_VALUE);
        if(jsonQuestion.has(JSON_TAG_ANSWER)){
            answer = new Answer(jsonQuestion.getJSONObject(JSON_TAG_ANSWER));
        }
    }
    @Override
    public int getQuestionType() {
        return QUESTION_TYPE;
    }
}
