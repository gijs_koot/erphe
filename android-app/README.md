# Resilience eBuddy app for Android #

This repository contains files that are associated with the *Resilience eBuddy* app for Android. 
This app enables users to answer questionnaires that are created on a webservice. Answers to questions are send to the webservice.

### Directory structure ###

* `app/scr/../database` This directory contains code related to the app's database (including several DAO's). The database structure is explained at the top of ```MySQLiteHelper.java```.
* `app/scr/../model` This directory contains code that defines several models (e.g., a Questionnaire and ListQuestion model). All question models extend ```AbstractQuestion.java```.
* `app/scr/../receivers` This directory holds several Broadcast Receivers that listen to certain events.
* `app/scr/../services` This directory contains code related to several (background) services (e.g, creating a notification or synchronizing results to the server).
* `app/scr/../utils` This directory contains some utilities / general code that is used throughout the app.

### Requirements ###
The requirements of the *Wi-Fi Monitor* app are mentioned below. Example projects may have their own requirements.

* Android Studio 1.2.1.1 (or another compatible IDE).
* Android SDK with API level 22.
* The app is targeted at API level 22, but requires at least API level 15 (this means devices running at least Android 4.0.3).
* The project has the following dependencies (downloaded using Gradle/Maven when using Android studio):
	* [```com.android.support:appcompat-v7:22.1.1```](https://developer.android.com/tools/support-library/features.html#v7-appcompat): This library adds support for the Action Bar user interface design pattern. 
	* [```com.android.support:support-v4:22.1.1```](https://developer.android.com/tools/support-library/features.html#v4): This library includes a large set of Android API's.

### Functionality ###

The app enables users to view and answer questionnaires. 
A questionnaire contains questions that need an answer (duh...). 
The first time the user opens the app he needs to create an account. 
Once the user is successfully authenticated, the app shows the list of questionnaires that are available. 
The user can open a questionnaires and answer its questions. 
Answers are directly sent to the webservice. 

#### Synchronization ####
Every 30 minutes the app retrieves the user’s questionnaires from the webservice. 
If a questionnaire’s due date has passed a notification will be created to inform the user about a new questionnaire.
This synchronization is defined in ```GetDataService.java```. 
Answers are directly sent to the webservice. This synchronization is defined in ```AbstractQuestionFragment.java```. 

### Who do I talk to? ###

This repository is maintained by [Thymen Wabeke](mailto:thymen.wabeke@tno.nl).
